<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*
Route::get('/', function () {
    return view('home');
});
*/

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/', 'HomeController@index')->name('home');


//Modulo Admision
Route::get('/citasDiarias', 'AdmisionController@citasDiarias')->name('citasDiarias');
Route::get('/citasSemanales', 'AdmisionController@citasSemanales')->name('citasSemanales');
Route::get('/citas', 'AdmisionController@citas')->name('citas');
Route::post('/buscarMedico', 'AdmisionController@buscarMedico')->name('buscarMedico');
Route::post('/buscarPaciente', 'AdmisionController@buscarPaciente')->name('buscarPaciente');
Route::post('/guardarPaciente', 'AdmisionController@guardarPaciente')->name('guardarPaciente');
Route::post('/buscarAcompaniante', 'AdmisionController@buscarAcompaniante')->name('buscarAcompaniante');
Route::post('/guardarAcompaniante', 'AdmisionController@guardarAcompaniante')->name('guardarAcompaniante');
Route::get('/eventosCalendar', 'AdmisionController@get_events')->name('eventosCalendar');
Route::post('/crearCita', 'AdmisionController@crearCita')->name('crearCita');

//Modulo Historias Clinicas
Route::get('/historiasClinicas', 'HistoriaclinicaController@historiasClinicas')->name('historiasClinicas');
Route::get('/pacientes', 'HistoriaclinicaController@pacientes')->name('pacientes');
Route::get('/listaHistoricaClinicas', 'HistoriaclinicaController@listaHistoricaClinicas')->name('listaHistoricaClinicas');
Route::get('/histclinicapaciente/{id}', 'HistoriaclinicaController@histclinicapaciente')->name('histclinicapaciente');
Route::post('/nuevoPaciente', 'HistoriaclinicaController@nuevoPaciente')->name('nuevoPaciente');

//Modulo Administracion
Route::get('/medicos', 'AdministracionController@medicos')->name('medicos');
Route::post('/nuevoMedico', 'AdministracionController@nuevoMedico')->name('nuevoMedico');
Route::get('/usuarios', 'AdministracionController@usuarios')->name('usuarios');
Route::get('/campanias', 'AdministracionController@campanias')->name('campanias');
Route::get('/inventario', 'AdministracionController@inventario')->name('inventario');
Route::get('/laboratorio', 'AdministracionController@laboratorio')->name('laboratorio');
Route::get('/concepto', 'AdministracionController@concepto')->name('concepto');
Route::get('/especialidad', 'AdministracionController@especialidad')->name('especialidad');
Route::get('/empresa', 'AdministracionController@empresa')->name('empresa');

//Modulo Laboratorio
Route::get('/faselab', 'LaboratorioController@faselab')->name('faselab');


//Modulo Control de Finanzas
Route::get('/pago', 'IngresoController@pago')->name('pago');
Route::get('/ventas', 'IngresoController@ventas')->name('ventas');
Route::get('/caja', 'IngresoController@caja')->name('caja');


//Reportes
Route::get('/controlPersonal', 'ReporteController@controlPersonal')->name('controlPersonal'); 