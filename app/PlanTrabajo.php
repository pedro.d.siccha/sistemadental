<?php

namespace SystemaDental;

use Illuminate\Database\Eloquent\Model;

class PlanTrabajo extends Model
{
    protected $table = 'plantrabajo';
    protected $fillable = ['id', 'fecha', 'estado', 'estadopago', 'total', 'diagnostico_id'];

    public static function pTraDia($id){
    	return PlanTrabajo::where('diagnostico_id', '=', $id) -> get();
    }
}
