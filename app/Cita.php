<?php

namespace SystemaDental;

use Illuminate\Database\Eloquent\Model;

class Cita extends Model
{
    protected $table = 'cita';
    protected $fillable = ['id', 'estado', 'detalle', 'paciente_id', 'medico_id', 'especialidad_id', 'acompaniante_id', 'start', 'end'];

    public static function citPac($id){
    	return Cita::where('paciente_id', '=', $id) -> get();
    }

    public static function citMed($id){
        return Cita::where('medico_id', '=', $id) -> get();
    }

    public static function citEsp($id){
        return Cita::where('especialidad_id', '=', $id) -> get();
    }

    public static function citAco($id){
        return Cita::where('acompaniante_id', '=', $id) -> get();
    }
}
