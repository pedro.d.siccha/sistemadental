<?php

namespace SystemaDental;

use Illuminate\Database\Eloquent\Model;

class Laboratorio extends Model
{
    protected $table = 'laboratorio';
    protected $fillable = ['id', 'nombre', 'ruc', 'encargado', 'direccion', 'estado'];

}
