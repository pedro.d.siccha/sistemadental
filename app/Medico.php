<?php

namespace SystemaDental;

use Illuminate\Database\Eloquent\Model;

class Medico extends Model
{
    protected $table = 'medico';
    protected $fillable = ['id', 'dni', 'numresgistro', 'nombre', 'apellido', 'fecnacimiento', 'edad', 'turno', 'estado', 'cargo', 'users_id', 'especialidad_id'];

    public static function medUse($id){
    	return Medico::where('users_id', '=', $id) -> get();
    }

    public static function medEsp($id){
        return Medico::where('especialidad_id', '=', $id) -> get();
    }
}
