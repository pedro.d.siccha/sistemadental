<?php

namespace SystemaDental;

use Illuminate\Database\Eloquent\Model;

class Acompaniante extends Model
{
    protected $table = 'acompaniante';
    protected $fillable = ['id', 'dni', 'nombre', 'apellido', 'direccion', 'referencia', 'telefono', 'fecnacimiento', 'edad', 'genero', 'correo', 'parentezo_id'];

    public static function acoPar($id){
        return Acompaniante::where('parentezo_id', '=', $id) -> get();
    }

}
