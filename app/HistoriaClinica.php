<?php

namespace SystemaDental;

use Illuminate\Database\Eloquent\Model;

class HistoriaClinica extends Model
{
    protected $table = 'historiaclinica';
    protected $fillable = ['id', 'estado', 'feccreacion', 'detalle', 'paciente_id'];

    public static function hClPac($id){
    	return HistoriaClinica::where('paciente_id', '=', $id) -> get();
    }
}
