<?php

namespace SystemaDental\Http\Controllers;

use Illuminate\Http\Request;
use SystemaDental\User;
use SystemaDental\Paciente;
use SystemaDental\HistoriaClinica;

class HistoriaclinicaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function historiasClinicas()
    {
        $historiaClinica = \DB::SELECT('SELECT hc.id AS historiaclinica_id, hc.estado, hc.feccreacion, p.nombre, p.apellido, p.dni, p.edad, p.correo, p.telefono 
                                        FROM historiaclinica hc, paciente p
                                        WHERE hc.paciente_id = p.id');

        return view('historiaclinica.historiasclinicas', compact('historiaClinica'));
    }

    public function pacientes()
    {
        $paciente = \DB::SELECT('SELECT * FROM paciente');

        return view('historiaclinica.pacientes', compact('paciente'));
    }

    public function listaHistoricaClinicas()
    {
        return view('historiaclinica.listahistoriasclinicas');
    }

    public function histclinicapaciente($id)
    {
        $historiaClinica = \DB::SELECT('SELECT hc.id AS historiaclinica_id, hc.estado, hc.feccreacion, p.nombre, p.apellido, p.dni, p.edad, p.correo, p.telefono 
                                        FROM historiaclinica hc, paciente p
                                        WHERE hc.paciente_id = p.id AND hc.id = "'.$id.'"');

        $historialCitas = \DB::SELECT('SELECT c.id AS cita_id, c.fecinicio as fecinicio, c.fecfin AS fecfin, c.estado, c.detalle, m.nombre, m.apellido, m.dni, e.nombre AS especialidad, a.nombre AS nomAcom, a.apellido AS apeAcom, a.dni AS dniAcom 
                                       FROM cita c
                                       INNER JOIN medico m ON c.medico_id = m.id
                                       INNER JOIN especialidad e ON c.especialidad_id = e.id
                                       INNER JOIN acompaniante a ON c.acompaniante_id = a.id
                                       INNER JOIN paciente p ON c.paciente_id = p.id
                                       LEFT JOIN historiaclinica hc ON hc.paciente_id = p.id
                                       WHERE hc.id = "'.$id.'"');

        $datosPersonales = \DB::SELECT('SELECT p.* 
                                        FROM paciente p, historiaclinica hc
                                        WHERE hc.paciente_id = p.id AND hc.id = "'.$id.'"');

        $diagnostico = \DB::SELECT('SELECT d.id AS diagnostico_id, d.descripcion, d.comentario 
                                    FROM cita c
                                    INNER JOIN diagnostico d ON d.cita_id = c.id
                                    INNER JOIN paciente p ON c.paciente_id = p.id
                                    LEFT JOIN historiaclinica hc ON hc.paciente_id = p.id
                                    WHERE hc.id = "'.$id.'"');

        $plantrabajo = \DB::SELECT('SELECT pt.id AS plantrabajo_id, pt.fecha, pt.estadopago, pt.total, e.nombre AS especialidad
                                    FROM cita c
                                    INNER JOIN diagnostico d ON d.cita_id = c.id
                                    LEFT JOIN plantrabajo pt ON pt.diagnostico_id = d.id
                                    LEFT JOIN especialidad e ON c.especialidad_id = e.id
                                    INNER JOIN paciente p ON c.paciente_id = p.id
                                    LEFT JOIN historiaclinica hc ON hc.paciente_id = p.id
                                    WHERE hc.id = "'.$id.'"');

        $evolucion = \DB::SELECT('SELECT ev.id AS evolucion_id, ev.fecha, e.nombre AS especialidad, ev.tratamiento, ev.observacion, m.nombre AS nomMedico, m.apellido AS apeMedico
                                  FROM cita c
                                  INNER JOIN medico m ON c.medico_id = m.id
                                  INNER JOIN diagnostico d ON d.cita_id = c.id
                                  LEFT JOIN plantrabajo pt ON pt.diagnostico_id = d.id
                                  LEFT JOIN evolucion ev ON ev.plantrabajo_id = pt.id
                                  LEFT JOIN especialidad e ON c.especialidad_id = e.id
                                  INNER JOIN paciente p ON c.paciente_id = p.id
                                  LEFT JOIN historiaclinica hc ON hc.paciente_id = p.id
                                  WHERE hc.id = "'.$id.'"');

        return view('historiaclinica.histclinicapaciente', compact('historiaClinica', 'historialCitas', 'datosPersonales', 'diagnostico', 'plantrabajo', 'evolucion'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function nuevoPaciente(Request $request)
    {
        $dni = $request->dni;
        $nombre = $request->nombre;
        $apellido = $request->apellido;
        $direccion = $request->direccion;
        $referencia = $request->referencia;
        $telefono = $request->telefono;
        $fecnacimiento = $request->fecnacimiento;
        $edad = $request->edad;
        $imagen = "";
        $genero = $request->genero;
        $correo = $request->correo;

        $user = new User();
        $user->name = $dni;
        $user->email = $correo;
        $user->password = bcrypt($dni);

         if ($user->save()) {
             
            $user = \DB::SELECT('SELECT MAX(id) AS id FROM users');

            $paciente = new Paciente();
            $paciente->dni = $dni;
            $paciente->nombre = $nombre;
            $paciente->apellido = $apellido;
            $paciente->direccion = $direccion;
            $paciente->referencia = $referencia;
            $paciente->telefono = $telefono;
            $paciente->fecnacimiento = $fecnacimiento;
            $paciente->edad = $edad;
            $paciente->imagen = "";
            $paciente->genero = $genero;
            $paciente->correo = $correo;
            $paciente->usuario = $dni;
            $paciente->users_id = $user[0]->id;

            if ($paciente->save()) {

                $paciente = \DB::SELECT('SELECT MAX(id) AS id FROM paciente');

                $nombrePaciente = $nombre." ".$apellido;

                $idPaciente = $paciente[0]->id;
                
                $hclinica = new HistoriaClinica();
                $hclinica->estado = "ACTIVO";
                $hclinica->feccreacion = date('Y-m-d');
                $hclinica->detalle = "";
                $hclinica->paciente_id = $idPaciente;

                if ($hclinica->save()) {

                    $paciente = \DB::SELECT('SELECT * FROM paciente');

                    return response()->json(["view"=>view('historiaclinica.divPaciente', compact('paciente'))->render()]);
                }

                
            }

         }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
