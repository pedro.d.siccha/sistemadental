<?php

namespace SystemaDental\Http\Controllers;

use Illuminate\Http\Request;
use SystemaDental\User;
use SystemaDental\Paciente;
use SystemaDental\Acompaniante;
use SystemaDental\HistoriaClinica;
use SystemaDental\Cita;

class AdmisionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function citasDiarias()
    {
        $medico = \DB::SELECT('SELECT m.id AS medico_id, m.nombre, m.apellido, m.dni, e.nombre as especialidad 
                               FROM medico m, especialidad e
                               WHERE m.especialidad_id = e.id AND e.estado = "ACTIVO"');

        $especialidad = \DB::SELECT('SELECT * FROM especialidad');

        $paciente = \DB::SELECT('SELECT * FROM paciente');

        $acompaniante = \DB::SELECT('SELECT * FROM acompaniante');

        $parentezco = \DB::SELECT('SELECT * FROM parentezo');

        return view('admision.citasdiarias', compact('medico', 'especialidad', 'paciente', 'acompaniante', 'parentezco'));
    }

    public function get_events()
    {
        $events =  \DB::SELECT('SELECT c.id, CONCAT(p.nombre, " ", p.apellido) AS title, CONCAT( m.nombre," ", m.apellido) AS descripcion, c.start, c.end
                                FROM cita c
                                INNER JOIN paciente p ON c.paciente_id = p.id
                                INNER JOIN medico m ON c.medico_id = m.id
                                INNER JOIN especialidad e ON c.especialidad_id = e.id
                                INNER JOIN acompaniante a ON c.acompaniante_id = a.id');

        return response()->json($events);


    }

    public function buscarMedico(Request $request)
    {
        $medico_id = $request->medico_id;

        $medico = \DB::SELECT('SELECT m.id AS medico_id, m.nombre, m.apellido, e.id AS especialidad_id 
                               FROM medico m, especialidad e 
                               WHERE m.especialidad_id = e.id AND m.id = "'.$medico_id.'"');

        $nombreMedico = $medico[0]->nombre." ".$medico[0]->apellido;

        $idMedico = $medico[0]->medico_id;

        $idEspecialidad = $medico[0]->especialidad_id;

        return response()->json(['nombreMedico'=>$nombreMedico, 'idMedico' => $idMedico, 'idEspecialidad'=>$idEspecialidad]);

    }

    public function buscarPaciente(Request $request)
    {
        $dni = $request->dni;

        $paciente = \DB::SELECT('SELECT id AS paciente_id, dni, nombre, apellido FROM paciente WHERE dni = "'.$dni.'"');

        if ($paciente != null) {
            
            $nombrePaciente = $paciente[0]->nombre. " ". $paciente[0]->apellido;

            $idPaciente = $paciente[0]->paciente_id;
        }else {
            $nombrePaciente = "0";

            $idPaciente = "0";
        }

        return response()->json(['nombrePaciente'=>$nombrePaciente, 'idPaciente' => $idPaciente]);

    }

    public function guardarPaciente(Request $request)
    {
        $dni = $request->dni;
        $nombre = $request->nombre;
        $apellido = $request->apellido;
        $direccion = $request->direccion;
        $referencia = $request->referencia;
        $telefono = $request->telefono;
        $fecnacimiento = $request->fecnacimiento;
        $edad = $request->edad;
        $imagen = "";
        $genero = $request->genero;
        $correo = $request->correo;

        $user = new User();
        $user->name = $dni;
        $user->email = $correo;
        $user->password = bcrypt($dni);

         if ($user->save()) {
             
            $user = \DB::SELECT('SELECT MAX(id) AS id FROM users');

            $paciente = new Paciente();
            $paciente->dni = $dni;
            $paciente->nombre = $nombre;
            $paciente->apellido = $apellido;
            $paciente->direccion = $direccion;
            $paciente->referencia = $referencia;
            $paciente->telefono = $telefono;
            $paciente->fecnacimiento = $fecnacimiento;
            $paciente->edad = $edad;
            $paciente->imagen = "";
            $paciente->genero = $genero;
            $paciente->correo = $correo;
            $paciente->usuario = $dni;
            $paciente->users_id = $user[0]->id;

            if ($paciente->save()) {

                $paciente = \DB::SELECT('SELECT MAX(id) AS id FROM paciente');

                $nombrePaciente = $nombre." ".$apellido;

                $idPaciente = $paciente[0]->id;
                
                $hclinica = new HistoriaClinica();
                $hclinica->estado = "ACTIVO";
                $hclinica->feccreacion = date('Y-m-d');
                $hclinica->detalle = "";
                $hclinica->paciente_id = $idPaciente;

                if ($hclinica->save()) {

                    return response()->json(['nombrePaciente'=>$nombrePaciente, 'idPaciente' => $idPaciente]);
                }

                
            }

         }
    }

    public function buscarAcompaniante(Request $request)
    {
        $dni = $request->dni;

        $acompaniante = \DB::SELECT('SELECT a.id AS acompaniante_id, a.nombre, a.apellido, p.nombre AS parentezco 
                                 FROM acompaniante a, parentezo p 
                                 WHERE a.parentezo_id = p.id AND a.dni = "'.$dni.'"');

        if ($acompaniante != null) {
            
            $nombreAcompaniante = $acompaniante[0]->nombre. " ". $acompaniante[0]->apellido;

            $parentezco = $acompaniante[0]->parentezco;

            $idAcompaniante = $acompaniante[0]->acompaniante_id;
            
        }else {
            $nombreAcompaniante = "0";

            $parentezco = "0";

            $idAcompaniante = "0";
        }

        return response()->json(['nombreAcompaniante'=>$nombreAcompaniante, 'idAcompaniante' => $idAcompaniante, 'parentezco' => $parentezco]);
    }

    public function guardarAcompaniante(Request $request)
    {
        $dni = $request->dni;
        $nombre = $request->nombre;
        $apellido = $request->apellido;
        $direccion = $request->direccion;
        $referencia = $request->referencia;
        $telefono = $request->telefono;
        $fecnacimiento = $request->fecnacimiento;
        $edad = $request->edad;
        $genero = $request->genero;
        $correo = $request->correo;
        $parentezco_id = $request->parentezco_id;

        $acom = new Acompaniante();
        $acom->dni = $dni;
        $acom->nombre = $nombre;
        $acom->apellido = $apellido;
        $acom->direccion = $direccion;
        $acom->referencia = $referencia;
        $acom->telefono = $telefono;
        $acom->fecnacimiento = $fecnacimiento;
        $acom->edad = $edad;
        $acom->genero = $genero;
        $acom->correo = $correo;
        $acom->parentezo_id = $parentezco_id;

        if ($acom->save()) {

            $acompaniante = \DB::SELECT('SELECT MAX(id) AS id FROM acompaniante');

            $parentezcoNom = \DB::SELECT('SELECT nombre FROM parentezco WHERE id = "'.$parentezco_id.'"');

            $idAcompaniante = $acompaniante[0]->id;

            $nombreAcompaniante = $nombre." ".$apellido;
            
            $parentezco = $parentezcoNom[0]->nombre;

            return response()->json(['nombreAcompaniante'=>$nombreAcompaniante, 'idAcompaniante' => $idAcompaniante, 'parentezco' => $parentezco]);

        }
    }

    public function crearCita(Request $request)
    {
        $start = $request->start;
        $end = $request->end;
        $detalle = $request->detalle;
        $dniPac = $request->dniPac;
        $dniAcom = $request->dniAcom;
        $nomMed = $request->medico;

        $paciente = \DB::SELECT('SELECT id FROM paciente WHERE dni = "'.$dniPac.'"');
        $acompaniante = \DB::SELECT('SELECT id FROM acompaniante WHERE dni = "'.$dniAcom.'"');
        $medico = \DB::SELECT('SELECT id FROM medico WHERE CONCAT(nombre, " ", apellido) = "'.$nomMed.'"');

        $especialidad_id = $request->especialidad_id;
        $paciente_id = $paciente[0]->id;
        $acompaniante_id = $acompaniante[0]->id;
        $medico_id = $medico[0]->id;

        $cit = new Cita();
        $cit->estado = "ACTIVO";
        $cit->detalle = $detalle;
        $cit->paciente_id = $paciente_id;
        $cit->medico_id = $medico_id;
        $cit->especialidad_id = $especialidad_id;
        $cit->acompaniante_id = $acompaniante_id;
        $cit->start = $start;
        $cit->end = $end;
        if ($cit->save()) {
           /* $events =  \DB::SELECT('SELECT c.id, CONCAT(p.nombre, " ", p.apellido) AS title, CONCAT( m.nombre," ", m.apellido) AS descripcion, c.start, c.end
                                FROM cita c
                                INNER JOIN paciente p ON c.paciente_id = p.id
                                INNER JOIN medico m ON c.medico_id = m.id
                                INNER JOIN especialidad e ON c.especialidad_id = e.id
                                INNER JOIN acompaniante a ON c.acompaniante_id = a.id');

            return response()->json($events);
            */
            return response()->json(["view"=>view('admision.nuevoCalendario')->render()]);

        }
    }

    public function citasSemanales()
    {
        return view('admision.citassemanales');
    }

    public function citas()
    {
        return view('admision.citas');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
