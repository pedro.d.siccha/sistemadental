<?php

namespace SystemaDental\Http\Controllers;

use Illuminate\Http\Request;
use SystemaDental\Medico;

class AdministracionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function medicos()
    {

        $medico = \DB::SELECT('SELECT m.*, e.nombre AS especialidad 
                               FROM medico m, especialidad e
                               WHERE m.especialidad_id = e.id');

        return view('administracion.medicos', compact('medico'));
    }

    public function nuevoMedico(Request $request)
    {

        $med = new Medico();
        $med->dni = $request->dni;
        $med->numregistro = $request->cmp;
        $med->nombre = $request->nombre;
        $med->apellido = $request->apellido;
        $med->direccion = $request->direccion;
        $med->telefono = $request->telefono;
        $med->fecnacimiento = $request->fecnacimiento;
        $med->edad = $request->edad;
        $med->turno = $request->turno;
        $med->estado = 1;
        $med->cargo = $request->cargo;
        $med->users_id = 2;
        $med->especialidad_id = $request->especialidad_id;

        if ($med->save()) {
            
            $paciente = \DB::SELECT('SELECT m.id AS medico_id, m.dni, m.numregistro, m.nombre, m.apellido, m.fecnacimiento, m.edad, m.turno, m.estado, m.cargo, e.id AS especialidad_id, e.nombre AS especialidad 
                                     FROM medico m, especialidad e
                                     WHERE m.especialidad_id = e.id');

            return response()->json(["view"=>view('administracion.divMedico', compact('medico'))->render()]);
            
        }

        $paciente = \DB::SELECT('SELECT m.id AS medico_id, m.dni, m.numregistro, m.nombre, m.apellido, m.fecnacimiento, m.edad, m.turno, m.estado, m.cargo, e.id AS especialidad_id, e.nombre AS especialidad 
                                 FROM medico m, especialidad e
                                 WHERE m.especialidad_id = e.id');

        return response()->json(["view"=>view('administracion.divMedico', compact('medico'))->render()]);

    }

    public function usuarios()
    {
        $usuario = \DB::SELECT('SELECT u.id AS users_id, u.name, u.email, m.nombre, m.apellido, m.cargo, m.estado 
                                FROM users u, medico m
                                WHERE m.users_id = u.id');

        return view('administracion.usuarios', compact('usuario'));
    } 

    public function campanias()
    {

        $campania = \DB::SELECT('SELECT * FROM campania');

        return view('administracion.campania', compact('campania'));
    }

    public function inventario()
    {

        $producto = \DB::SELECT('SELECT * FROM producto');

        return view('administracion.inventario', compact('producto'));
    }

    public function laboratorio()
    {
        $laboratorio = \DB::SELECT('SELECT * FROM laboratorio');

        return view('administracion.laboratorios', compact('laboratorio'));
    }

    public function concepto()
    {
        $servicio = \DB::SELECT('SELECT * FROM servicio');

        return view('administracion.concepto', compact('servicio'));
    }

    public function especialidad()
    {

        $especialidad = \DB::SELECT('SELECT * FROM especialidad');

        return view('administracion.especialidad', compact('especialidad'));
    }

    public function empresa()
    {
        $empresa = \DB::SELECT('SELECT * FROM proveedor');

        return view('administracion.empresa', compact('empresa'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
