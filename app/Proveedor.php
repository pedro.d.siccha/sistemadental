<?php

namespace SystemaDental;

use Illuminate\Database\Eloquent\Model;

class Proveedor extends Model
{
    protected $table = 'proveedor';
    protected $fillable = ['id', 'estado', 'nombre', 'direccion', 'ruc', 'encargado', 'telefono', 'correo', 'detalle'];

}
