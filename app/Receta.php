<?php

namespace SystemaDental;

use Illuminate\Database\Eloquent\Model;

class Receta extends Model
{
    protected $table = 'receta';
    protected $fillable = ['id', 'fecha', 'descripcion', 'indicacion', 'diagnostico_id', 'producto_id'];

    public static function recDia($id){
    	return Receta::where('diagnostico_id', '=', $id) -> get();
    }

    public static function recPro($id){
        return Receta::where('producto_id', '=', $id) -> get();
    }
}
