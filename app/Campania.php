<?php

namespace SystemaDental;

use Illuminate\Database\Eloquent\Model;

class Campania extends Model
{
    protected $table = 'campania';
    protected $fillable = ['id', 'estado', 'fecregistro', 'fecinicio', 'titulo'];

}
