<?php

namespace SystemaDental;

use Illuminate\Database\Eloquent\Model;

class Especialidad extends Model
{
    protected $table = 'especialidad';
    protected $fillable = ['id', 'estado', 'nombre', 'detalle'];

}
