<?php

namespace SystemaDental;

use Illuminate\Database\Eloquent\Model;

class Producto extends Model
{
    protected $table = 'producto';
    protected $fillable = ['id', 'nombre', 'stockminimo', 'stockactual', 'precio', 'descripcion', 'estado', 'tipoproducto_id'];

    public static function proTPr($id){
    	return Producto::where('tipoproducto_id', '=', $id) -> get();
    }
}
