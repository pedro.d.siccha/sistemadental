<?php

namespace SystemaDental;

use Illuminate\Database\Eloquent\Model;

class Servicio extends Model
{
    protected $table = 'servicio';
    protected $fillable = ['id', 'estado', 'tiposervicio', 'nombre', 'precio', 'fecregistro', 'medico'];

}
