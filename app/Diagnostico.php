<?php

namespace SystemaDental;

use Illuminate\Database\Eloquent\Model;

class Diagnostico extends Model
{
    protected $table = 'diagnostico';
    protected $fillable = ['id', 'descripcion', 'comentario', 'fecha', 'hora', 'cita_id'];

    public static function diaCit($id){
    	return Diagnostico::where('cita_id', '=', $id) -> get();
    }
}
