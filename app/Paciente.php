<?php

namespace SystemaDental;

use Illuminate\Database\Eloquent\Model;

class Paciente extends Model
{
    protected $table = 'paciente';
    protected $fillable = ['id', 'dni', 'nombre', 'apellido', 'direccion', 'referencia', 'telefono', 'fecnacimiento', 'edad', 'imagen', 'genero', 'correo', 'users_id'];

    public static function pacUse($id){
    	return Paciente::where('users_id', '=', $id) -> get();
    }
}
