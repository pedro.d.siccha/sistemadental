<?php

namespace SystemaDental;

use Illuminate\Database\Eloquent\Model;

class TipoProducto extends Model
{
    protected $table = 'tipoproducto';
    protected $fillable = ['id', 'nombre', 'descripcion'];

}
