<?php

namespace SystemaDental;

use Illuminate\Database\Eloquent\Model;

class Evolucion extends Model
{
    protected $table = 'evolucion';
    protected $fillable = ['id', 'fecha', 'tratamiento', 'observacion', 'estado', 'plantrabajo_id'];

    public static function evoPTr($id){
    	return Evolucion::where('plantrabajo_id', '=', $id) -> get();
    }
}
