@extends('layouts.app')
@section('nombrePagina')
 Pacientes
@endsection
@section('contenido')
<div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
        <div class="x_title">
        <h2>Lista de Pacientes <button type="button" class="btn btn-default btn-xs" data-toggle="tooltip" data-placement="top" title="Registrar Paciente" onclick="modalNuevoPaciente()"><i class="fa fa-plus"></i></button></h2>
        <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
            <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
            <ul class="dropdown-menu" role="menu">
                <li><a href="#">Settings 1</a>
                </li>
                <li><a href="#">Settings 2</a>
                </li>
            </ul>
            </li>
            <li><a class="close-link"><i class="fa fa-close"></i></a>
            </li>
        </ul>
        <div class="clearfix"></div>
        </div>
        <div class="x_content" id="divPaciente">
        <table id="datatable" class="table table-striped table-bordered">
            <thead>
            <tr>
                <th>Cod.</th>
                <th>Nombres</th>
                <th>DNI</th>
                <th>Fec. Nacimiento</th>
                <th>Edad</th>
                <th>Gestion</th>
            </tr>
            </thead>
            <tbody>
                @foreach ($paciente as $p)
                <tr>
                    <td>{{ $p->id }}</td>
                    <td>{{ $p->nombre }} {{ $p->apellido }}</td>
                    <td>{{ $p->dni }}</td>
                    <td>{{ $p->fecnacimiento }}</td>
                    <td>{{ $p->edad }} años</td>
                    <td><button type="button" class="btn btn-default btn-xs" data-toggle="tooltip" data-placement="top" title="Generar Historia Clinica" disabled="disabled"><i class="fa fa-calendar-o"></i></button><button type="button" class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="top" title="Eliminar Paciente" disabled="disabled"><i class="fa fa-trash"></i></button></td>
                </tr>
                @endforeach
            </tbody>
        </table>
        </div>
    </div>
</div>

<!-- Modal Crear Paciente -->
<div id="crearPaciente" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
            </button>
            <h4 class="modal-title" id="myModalLabel">Crear Nuevo Paciente</h4>
        </div>
        <div class="modal-body row">
            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                <label class="control-label col-md-3 col-sm-3 col-xs-6">DNI</label>
                <input type="text" class="form-control has-feedback-left" id="dniNuevo" placeholder="Nuevo DNI">
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                <label class="control-label col-md-3 col-sm-3 col-xs-6">Nombre</label>
                <input type="text" class="form-control has-feedback-left" id="nombreNuevo" placeholder="Ingrese Nombre">
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                <label class="control-label col-md-3 col-sm-3 col-xs-6">Apellido</label>
                <input type="text" class="form-control has-feedback-left" id="apellidoNuevo" placeholder="Ingrese Apellido">
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                <label class="control-label col-md-3 col-sm-3 col-xs-6">Direccion</label>
                <input type="text" class="form-control has-feedback-left" id="direccionNuevo" placeholder="Ingrese Dirección">
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                <label class="control-label col-md-3 col-sm-3 col-xs-6">Referencia</label>
                <input type="text" class="form-control has-feedback-left" id="referenciaNuevo" placeholder="Ingrese Referencia">
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                <label class="control-label col-md-3 col-sm-3 col-xs-6">Teléfono</label>
                <input type="text" class="form-control has-feedback-left" id="telefonoNuevo" placeholder="Ingresar Teléfono">
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                <label class="control-label col-md-6 col-sm-6 col-xs-6">Fecha de Nacimiento</label>
                <input type="date" class="form-control has-feedback-left" id="fecnacimientoNuevo">
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                <label class="control-label col-md-3 col-sm-3 col-xs-6">Edad</label>
                <input type="text" class="form-control has-feedback-left" id="edadNuevo" placeholder="Ingrese Edad">
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                <label class="control-label col-md-3 col-sm-3 col-xs-6">Género</label>
                <input type="text" class="form-control has-feedback-left" id="generoNuevo" placeholder="Ingresar Género">
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                <label class="control-label col-md-3 col-sm-3 col-xs-6">Correo</label>
                <input type="text" class="form-control has-feedback-left" id="correoNuevo" placeholder="Ingrese Correo">
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            <button type="button" class="btn btn-primary" onclick="nuevoPaciente()">Guardar</button>
        </div>

        </div>
    </div>
</div>
<!-- Fin Modal Crear Paciente -->
@endsection
@section('script')
    <script>
        function modalNuevoPaciente() {
            $('#crearPaciente').modal('show');
        }

        function nuevoPaciente() {
               var dni = $('#dniNuevo').val();
               var nombre = $('#nombreNuevo').val();
               var apellido = $('#apellidoNuevo').val();
               var direccion = $('#direccionNuevo').val();
               var referencia = $('#referenciaNuevo').val();
               var telefono = $('#telefonoNuevo').val();
               var fecnacimiento = $('#fecnacimientoNuevo').val();
               var edad = $('#edadNuevo').val();
               var genero = $('#generoNuevo').val();
               var correo = $('#correoNuevo').val();

               $.post( "{{ Route('nuevoPaciente') }}", {dni: dni, nombre: nombre, apellido: apellido, direccion: direccion, referencia: referencia, telefono: telefono, fecnacimiento: fecnacimiento, edad: edad, genero: genero, correo: correo, _token:'{{csrf_token()}}'}).done(function(data) {
                        $("#divPaciente").empty();
                        $("#divPaciente").html(data.view);        
                        $('#crearPaciente').modal('hide');
                   });
           }
    </script>
@endsection