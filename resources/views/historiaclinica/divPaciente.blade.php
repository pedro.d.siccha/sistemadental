<table id="datatable" class="table table-striped table-bordered">
    <thead>
    <tr>
        <th>Cod.</th>
        <th>Nombres</th>
        <th>DNI</th>
        <th>Fec. Nacimiento</th>
        <th>Edad</th>
        <th>Gestion</th>
    </tr>
    </thead>
    <tbody>
        @foreach ($paciente as $p)
        <tr>
            <td>{{ $p->id }}</td>
            <td>{{ $p->nombre }} {{ $p->apellido }}</td>
            <td>{{ $p->dni }}</td>
            <td>{{ $p->fecnacimiento }}</td>
            <td>{{ $p->edad }} años</td>
            <td><button type="button" class="btn btn-default btn-xs" data-toggle="tooltip" data-placement="top" title="Generar Historia Clinica" disabled="disabled"><i class="fa fa-calendar-o"></i></button><button type="button" class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="top" title="Eliminar Paciente" disabled="disabled"><i class="fa fa-trash"></i></button></td>
        </tr>
        @endforeach
    </tbody>
</table>