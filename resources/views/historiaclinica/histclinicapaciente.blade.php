@extends('layouts.app')
@section('nombrePagina')
 Paciente
@endsection
@section('contenido')
<div role="main">
    <div class="">
        <div class="page-title">
        <div class="title_left">
        </div>
        </div>
        
        <div class="clearfix"></div>

        <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
            <div class="x_title">
                <ul class="nav navbar-right panel_toolbox">
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                    <ul class="dropdown-menu" role="menu">
                    <li><a href="#">Settings 1</a>
                    </li>
                    <li><a href="#">Settings 2</a>
                    </li>
                    </ul>
                </li>
                <li><a class="close-link"><i class="fa fa-close"></i></a>
                </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="col-md-3 col-sm-3 col-xs-12 profile_left">
                    @foreach ($historiaClinica as $hc)
                    <div class="profile_img">
                        <div id="crop-avatar">
                        <!-- Current avatar -->
                        <img class="img-responsive avatar-view" src="{{asset('production/images/picture.jpg')}}" alt="Avatar" title="Change the avatar">
                        </div>
                    </div>
                    <h3>{{ $hc->historiaclinica_id }}</h3>

                    <ul class="list-unstyled user_data">
                        <li><i class="fa fa-map-marker user-profile-icon"></i> {{ $hc->nombre }} {{ $hc->apellido }}
                        </li>

                        <li>
                        <i class="fa fa-briefcase user-profile-icon"></i> {{ $hc->dni }}
                        </li>

                        <li class="m-top-xs">
                        <i class="fa fa-external-link user-profile-icon"></i>
                        <a href="http://www.kimlabs.com/profile/" target="_blank">{{ $hc->edad }} años</a>
                        </li>
                    </ul>

                    <a class="btn btn-success"><i class="fa fa-edit m-right-xs"></i>Editar Datos</a>
                    <br />
                    @endforeach
                </div>
                <div class="col-md-9 col-sm-9 col-xs-12">
                <div class="" role="tabpanel" data-example-id="togglable-tabs">
                    <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#tab_anamnesis" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Anamnesis</a>
                        </li>
                        <li role="presentation" class=""><a href="#tab_diagnostico" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">Diagnostico</a>
                        </li>
                        <li role="presentation" class=""><a href="#tab_odontograma" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">Odontograma</a>
                        </li>
                        <li role="presentation" class=""><a href="#tab_plantrabajo" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">Plan de Trabajo</a>
                        </li>
                        <li role="presentation" class=""><a href="#tab_evolucion" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">Evolucion</a>
                        </li>
                        <li role="presentation" class=""><a href="#tab_recetas" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">Recetas</a>
                        </li>
                        <li role="presentation" class=""><a href="#tab_citas" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">Citas</a>
                        </li>
                    </ul>
                    <div id="myTabContent" class="tab-content">
                    <div role="tabpanel" class="tab-pane fade active in" id="tab_anamnesis" aria-labelledby="home-tab">

                            <div class="clearfix"></div>
                            <div class="row">
                              <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel">
                                  <div class="x_title">
                                    <h2>Historial de Citas</h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                      </li>
                                      <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                        <ul class="dropdown-menu" role="menu">
                                          <li><a href="#">Settings 1</a>
                                          </li>
                                          <li><a href="#">Settings 2</a>
                                          </li>
                                        </ul>
                                      </li>
                                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                                      </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                  </div>
                                  <div class="x_content">
                                    <br />
                                    <table class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Fecha de Cita</th>
                                            <th>Estado de Cita</th>
                                            <th>Gestion</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($historialCitas as $hc)
                                            <tr>
                                                <th scope="row"></th>
                                                <td>{{ $hc->fecinicio }}</td>
                                                <td>{{ $hc->estado }}</td>
                                                <td><button type="button" class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="top" title="Eliminar Alergia"><i class="fa fa-trash"></i></button></td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                  </div>
                                </div>
                              </div>
                            </div>

                            <div class="clearfix"></div>
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>Datos del Paciente </h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                    </li>
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                        <ul class="dropdown-menu" role="menu">
                                        <li><a href="#">Settings 1</a>
                                        </li>
                                        <li><a href="#">Settings 2</a>
                                        </li>
                                        </ul>
                                    </li>
                                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                                    </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    @foreach ($datosPersonales as $dp)
                                    <div class="form-horizontal form-label-left input_mask">
                
                                        <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                            <label class="control-label col-md-4 col-sm-4 col-xs-12" for="last-name">N° Documento 
                                            </label>
                                            <input type="text" class="form-control has-feedback-left" value="{{ $dp->dni }}" readonly>
                                        </div>
                    
                                        <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Nombres 
                                            </label>
                                            <input type="text" class="form-control" value="{{ $dp->nombre }}" readonly>
                                        </div>
                    
                                        <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Apellidos 
                                            </label>
                                            <input type="text" class="form-control has-feedback-left" value="{{ $dp->apellido }}" readonly>
                                        </div>
                    
                                        <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Direccion 
                                            </label>
                                            <input type="text" class="form-control" value="{{ $dp->direccion }}" readonly>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Referencia 
                                            </label>
                                            <input type="text" class="form-control has-feedback-left" value="{{ $dp->referencia }}" readonly>
                                        </div>
                    
                                        <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Teléfono
                                            </label>
                                            <input type="text" class="form-control" value="{{ $dp->telefono }}" readonly>
                                        </div>
                    
                                        <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                            <label class="control-label col-md-5 col-sm-5 col-xs-12" for="last-name">Fecha de Nacimiento
                                            </label>
                                            <input type="date" class="form-control has-feedback-left" value="{{ $dp->fecnacimiento }}" readonly>
                                        </div>
                    
                                        <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Edad
                                            </label>
                                            <input type="text" class="form-control" value="{{ $dp->edad }}" readonly>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Genero
                                            </label>
                                            <input type="text" class="form-control has-feedback-left" value="{{ $dp->genero }}" readonly>
                                        </div>
                    
                                        <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Correo
                                            </label>
                                            <input type="text" class="form-control" value="{{ $dp->correo }}" readonly>
                                        </div>
                                        <div class="ln_solid"></div>
                
                                     </div>
                                    @endforeach
                                    
                                </div>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel">
                                    <div class="x_title">
                                    <h2>Alergias</h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                        </li>
                                        <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a href="#">Settings 1</a>
                                            </li>
                                            <li><a href="#">Settings 2</a>
                                            </li>
                                        </ul>
                                        </li>
                                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                    </div>
                                    <div class="x_content">
                
                                    <table class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Nombre</th>
                                            <th>Descripcion</th>
                                            <th>Gestionar</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <th scope="row">1</th>
                                            <td>Ibuprofeno</td>
                                            <td></td>
                                            <td><button type="button" class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="top" title="Eliminar Alergia"><i class="fa fa-trash"></i></button></td>
                                        </tr>
                                        </tbody>
                                    </table>
                
                                    </div>
                                </div>
                            </div>
                    </div>

                    <div role="tabpanel" class="tab-pane fade" id="tab_diagnostico" aria-labelledby="profile-tab">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel">
                                    <div class="x_title">
                                    <h2>Diagnosticos</h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                        </li>
                                        <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a href="#">Settings 1</a>
                                            </li>
                                            <li><a href="#">Settings 2</a>
                                            </li>
                                        </ul>
                                        </li>
                                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                    </div>
                                    <div class="x_content">
                
                                    <table class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Codigo</th>
                                            <th>Descripcion</th>
                                            <th>Comentario</th>
                                            <th>Gestion</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($diagnostico as $d)
                                            <tr>
                                                <th scope="row"></th>
                                                <td>{{ $d->diagnostico_id }}</td>
                                                <td>{{ $d->descripcion }}</td>
                                                <td>{{ $d->comentario }}</td>
                                                <td><button type="button" class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="top" title="Eliminar Alergia"><i class="fa fa-trash"></i></button></td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                
                                    </div>
                                </div>
                            </div>
                    </div>

                    <div role="tabpanel" class="tab-pane fade" id="tab_odontograma" aria-labelledby="profile-tab">
                        <!-- Dibujar odontograma -->
                        <div id="radio">
                            <input type="radio" id="radio1" name="accion" value="fractura" checked="checked" /><label for="radio1">Fractura</label>
                            <input type="radio" id="radio2" name="accion" value="restauracion" /><label for="radio2">Restauracion</label>
                            <input type="radio" id="radio3" name="accion" value="borrar" /><label for="radio3">Borrar</label>
                            <input type="radio" id="radio4" name="accion" value="extraccion" /><label for="radio4">Extraccion</label>
                            <input type="radio" id="radio5" name="accion" value="puente" /><label for="radio5">Puente</label>
                        </div>

                        <div id="canvasesdiv" style="position:relative; width:810px; height:300px">
                            <canvas id="myCanvas" width="810" height="300" style="z-index: 1; position:absolute; left:0px; top:0px;"></canvas>
                            <canvas id="myCanvas2" width="810" height="300" style="z-index: 2; position:absolute; left:0px; top:0px;"></canvas>
                            <canvas id="myCanvas3" width="810" height="300" style="z-index: 3; position:absolute; left:0px; top:0px;"></canvas>
                            <canvas id="myCanvas4" width="810" height="300" style="z-index: 4; position:absolute; left:0px; top:0px;"></canvas>
                        </div>

                        <div id="radio_seccion" style='display:none'>
                            <input type="radio" id="radio_1" name="seccion" value="seccion" checked="checked" /><label for="radio_1">Seccion</label>
                            <input type="radio" id="radio_2" name="seccion" value="diente" /><label for="radio_2">Diente</label>
                        </div>





                        <!-- Fin Dibujo odontograma -->
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="tab_plantrabajo" aria-labelledby="profile-tab">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel">
                                    <div class="x_title">
                                    <h2>Plan de Trabajo</h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                        </li>
                                        <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a href="#">Settings 1</a>
                                            </li>
                                            <li><a href="#">Settings 2</a>
                                            </li>
                                        </ul>
                                        </li>
                                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                    </div>
                                    <div class="x_content">
                
                                    <table class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Fecha</th>
                                            <th>Estado Pago</th>
                                            <th>Codigo</th>
                                            <th>Especialidad</th>
                                            <th>Total</th>
                                            <th>Gestion</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($plantrabajo as $pt)
                                            <tr>
                                                <th scope="row"></th>
                                                <td>{{ $pt->fecha }}</td>
                                                <td>{{ $pt->estadopago }}</td>
                                                <td>{{ $pt->plantrabajo_id }}</td>
                                                <td>{{ $pt->especialidad }}</td>
                                                <td>S/. {{ $pt->total }}</td>
                                                <td><button type="button" class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="top" title="Eliminar Alergia"><i class="fa fa-trash"></i></button></td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                
                                    </div>
                                </div>
                            </div>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="tab_evolucion" aria-labelledby="profile-tab">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel">
                                    <div class="x_title">
                                    <h2>Evolucion</h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                        </li>
                                        <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a href="#">Settings 1</a>
                                            </li>
                                            <li><a href="#">Settings 2</a>
                                            </li>
                                        </ul>
                                        </li>
                                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                    </div>
                                    <div class="x_content">
                
                                    <table class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Fecha</th>
                                            <th>Especialidad</th>
                                            <th>Médico</th>
                                            <th>Tratamiento</th>
                                            <th>Observacion</th>
                                            <th>Gestion</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($evolucion as $e)
                                            <tr>
                                                <th scope="row"></th>
                                                <td>{{ $e->fecha }}</td>
                                                <td>{{ $e->especialidad }}</td>
                                                <td>{{ $e->nomMedico }} {{ $e->apeMedico }}</td>
                                                <td>{{ $e->tratamiento }}</td>
                                                <td>{{ $e->observacion }}</td>
                                                <td><button type="button" class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="top" title="Eliminar Alergia"><i class="fa fa-trash"></i></button></td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                
                                    </div>
                                </div>
                            </div>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="tab_recetas" aria-labelledby="profile-tab">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel">
                                    <div class="x_title">
                                    <h2>Receta</h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                        </li>
                                        <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a href="#">Settings 1</a>
                                            </li>
                                            <li><a href="#">Settings 2</a>
                                            </li>
                                        </ul>
                                        </li>
                                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                    </div>
                                    <div class="x_content">
                
                                    <table class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Fecha</th>
                                            <th>Descripcion</th>
                                            <th>Gestion</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <th scope="row"></th>
                                            <td>16/11/2019</td>
                                            <td>Amoxixilina</td>
                                            <td><button type="button" class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="top" title="Eliminar Alergia"><i class="fa fa-trash"></i></button></td>
                                        </tr>
                                        </tbody>
                                    </table>
                
                                    </div>
                                </div>
                            </div>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="tab_citas" aria-labelledby="profile-tab">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel">
                                    <div class="x_title">
                                    <h2>Citas</h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                        </li>
                                        <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a href="#">Settings 1</a>
                                            </li>
                                            <li><a href="#">Settings 2</a>
                                            </li>
                                        </ul>
                                        </li>
                                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                    </div>
                                    <div class="x_content">
                
                                    <table class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Fecha</th>
                                            <th>Hora Inicio</th>
                                            <th>Hora Fin</th>
                                            <th>Estado Cita</th>
                                            <th>Médico</th>
                                            <th>Gestion</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <th scope="row"></th>
                                            <td>16/11/2019</td>
                                            <td>16:00</td>
                                            <td>17:00</td>
                                            <td>Sala Espera</td>
                                            <td>Bruno Nash</td>
                                            <td><button type="button" class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="top" title="Eliminar Alergia"><i class="fa fa-trash"></i></button></td>
                                        </tr>
                                        </tbody>
                                    </table>
                
                                    </div>
                                </div>
                            </div>
                    </div>
                    </div>
                </div>
                </div>
            </div>
            </div>
        </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<style>
    #myCanvas {
        border: 1px solid #9C9898;
    }
    #myCanvas2 {
        border: 1px solid #9C9898;
    }
    #myCanvas3 {
        border: 1px solid #9C9898;
    }
    #myCanvas4 {
        border: 1px solid #9C9898;
    }
    body{
        text-align: center;
    }
</style>
<style  type="text/css">
    body, a, a:hover {cursor:  url(cur438.cur),  progress;}
</style>
<script>
    $(function() {
        $( "#radio" ).buttonset();
        $( "#radio_seccion" ).buttonset();
        $('#radio').change(function () {
            accion = $("input[name='accion']:checked").val();
            if(accion=='borrar'){
                $("#radio_seccion").show( 'blind', 500 );
            }else{
                $("#radio_seccion").hide( );
            }
        });
    });
</script>

<script>
    // Funcion para dibujar las lineas de cada diente
    function dibuja_contorno(context, inicio_x, inicio_y, med, separacion_x, separacion_y){
        var ctx = context;
        // Definiendo puntos de dibujo
        cua = med/4;
        ter = cua*3;
        /* 1ra zona */
        if(ctx){
            ctx.strokeStyle = color_line;
            ctx.beginPath();
            ctx.moveTo(inicio_x,inicio_y);
            ctx.lineTo(med+inicio_x,inicio_y);
            ctx.lineTo(ter+inicio_x,cua+inicio_y);
            ctx.lineTo(cua+inicio_x,cua+inicio_y);
            ctx.closePath();
            ctx.stroke();
        }
        /* 2da zona */
        if(ctx){
            ctx.strokeStyle = color_line;
            ctx.beginPath();
            ctx.moveTo(ter+inicio_x,cua+inicio_y);
            ctx.lineTo(med+inicio_x,inicio_y);
            ctx.lineTo(med+inicio_x,med+inicio_y);
            ctx.lineTo(ter+inicio_x,ter+inicio_y);
            ctx.closePath();
            ctx.stroke();
        }
        /* 3ra zona */
        if(ctx){
            ctx.strokeStyle = color_line;
            ctx.beginPath();
            ctx.moveTo(cua+inicio_x,ter+inicio_y);
            ctx.lineTo(ter+inicio_x,ter+inicio_y);
            ctx.lineTo(med+inicio_x,med+inicio_y);
            ctx.lineTo(inicio_x,med+inicio_y);
            ctx.closePath();
            ctx.stroke();
        }
        /* 4ta zona */
        if(ctx){
            ctx.strokeStyle = color_line;
            ctx.beginPath();
            ctx.moveTo(inicio_x,inicio_y);
            ctx.lineTo(cua+inicio_x,cua+inicio_y);
            ctx.lineTo(cua+inicio_x,ter+inicio_y);
            ctx.lineTo(inicio_x,med+inicio_y);
            ctx.closePath();
            ctx.stroke();
        }
    }
    // Funcion para pintar una region del diente
    function dibuja_seccion(contexto, num_diente, seccion, color_pas){
        var ctx = contexto;
        // Definiendo puntos de dibujo
        med = medida;
        cua = med/4;
        ter = cua*3;
        num_diente = num_diente - 1;
        color_line = color_pas;
        if (num_diente<16){
            inicio_y = 20;
        }
        else{
            num_diente = num_diente - 16;
            inicio_y = med + 100;
            //if(num_diente==1){num_diente=0;}
        }
        //alert(num_diente);
        inicio_x = (num_diente*med) + (separacion_x*num_diente) + separacion_x;
        /* 1ra zona */
        if (seccion==1){
            if(ctx){
                ctx.fillStyle = color_line;
                ctx.beginPath();
                ctx.moveTo(inicio_x,inicio_y);
                ctx.lineTo(med+inicio_x,inicio_y);
                ctx.lineTo(ter+inicio_x,cua+inicio_y);
                ctx.lineTo(cua+inicio_x,cua+inicio_y);
                ctx.closePath();
                ctx.fill();
                ctx.strokeStyle = 'black';
                ctx.stroke();
            }
        }
        /* 2da zona */
        if (seccion==2){
            if(ctx){
                ctx.fillStyle = color_line;
                ctx.beginPath();
                ctx.moveTo(ter+inicio_x,cua+inicio_y);
                ctx.lineTo(med+inicio_x,inicio_y);
                ctx.lineTo(med+inicio_x,med+inicio_y);
                ctx.lineTo(ter+inicio_x,ter+inicio_y);
                ctx.closePath();
                ctx.fill();
                ctx.strokeStyle = 'black';
                ctx.stroke();
            }
        }
        /* 3ra zona */
        if (seccion==3){
            if(ctx){
                ctx.fillStyle = color_line;
                ctx.beginPath();
                ctx.moveTo(cua+inicio_x,ter+inicio_y);
                ctx.lineTo(ter+inicio_x,ter+inicio_y);
                ctx.lineTo(med+inicio_x,med+inicio_y);
                ctx.lineTo(inicio_x,med+inicio_y);
                ctx.closePath();
                ctx.fill();
                ctx.strokeStyle = 'black';
                ctx.stroke();
            }
        }
        /* 4ta zona */
        if (seccion==4){
            if(ctx){
                ctx.fillStyle = color_line;
                ctx.beginPath();
                ctx.moveTo(inicio_x,inicio_y);
                ctx.lineTo(cua+inicio_x,cua+inicio_y);
                ctx.lineTo(cua+inicio_x,ter+inicio_y);
                ctx.lineTo(inicio_x,med+inicio_y);
                ctx.closePath();
                ctx.fill();
                ctx.strokeStyle = 'black';
                ctx.stroke();
            }
        }
        /* 5ta zona(medio) */
        if (seccion==5){
            if(ctx){
                ctx.fillStyle = color_line;
                ctx.beginPath();
                ctx.moveTo(cua+inicio_x,cua+inicio_y);
                ctx.lineTo(ter+inicio_x,cua+inicio_y);
                ctx.lineTo(ter+inicio_x,ter+inicio_y);
                ctx.lineTo(cua+inicio_x,ter+inicio_y);
                ctx.closePath();
                ctx.fill();
                ctx.strokeStyle = 'black';
                ctx.stroke();
            }
        }
    }
    //
    // Funcion para sombrear
    function marcar_seccion(contexto, num_diente, seccion, color_pas){
        var ctx = contexto;
        // Definiendo puntos de dibujo
        med = medida;
        cua = med/4;
        ter = cua*3;
        num_diente = num_diente - 1;
        color_line = color_pas;
        if (num_diente<16){
            inicio_y = 20;
        }
        else{
            num_diente = num_diente - 16;
            inicio_y = med + 100;
            //if(num_diente==1){num_diente=0;}
        }
        //alert(num_diente);
        inicio_x = (num_diente*med) + (separacion_x*num_diente) + separacion_x;
        /* 1ra zona */
        if (seccion==1){
            if(ctx){
                ctx.fillStyle = color_line;
                ctx.beginPath();
                ctx.moveTo(inicio_x,inicio_y);
                ctx.lineTo(med+inicio_x,inicio_y);
                ctx.lineTo(ter+inicio_x,cua+inicio_y);
                ctx.lineTo(cua+inicio_x,cua+inicio_y);
                ctx.closePath();
                //ctx.fill();
                ctx.strokeStyle = 'yellow';
                ctx.stroke();
            }
        }
        /* 2da zona */
        if (seccion==2){
            if(ctx){
                ctx.fillStyle = color_line;
                ctx.beginPath();
                ctx.moveTo(ter+inicio_x,cua+inicio_y);
                ctx.lineTo(med+inicio_x,inicio_y);
                ctx.lineTo(med+inicio_x,med+inicio_y);
                ctx.lineTo(ter+inicio_x,ter+inicio_y);
                ctx.closePath();
                //ctx.fill();
                ctx.strokeStyle = 'yellow';
                ctx.stroke();
            }
        }
        /* 3ra zona */
        if (seccion==3){
            if(ctx){
                ctx.fillStyle = color_line;
                ctx.beginPath();
                ctx.moveTo(cua+inicio_x,ter+inicio_y);
                ctx.lineTo(ter+inicio_x,ter+inicio_y);
                ctx.lineTo(med+inicio_x,med+inicio_y);
                ctx.lineTo(inicio_x,med+inicio_y);
                ctx.closePath();
                //ctx.fill();
                ctx.strokeStyle = 'yellow';
                ctx.stroke();
            }
        }
        /* 4ta zona */
        if (seccion==4){
            if(ctx){
                ctx.fillStyle = color_line;
                ctx.beginPath();
                ctx.moveTo(inicio_x,inicio_y);
                ctx.lineTo(cua+inicio_x,cua+inicio_y);
                ctx.lineTo(cua+inicio_x,ter+inicio_y);
                ctx.lineTo(inicio_x,med+inicio_y);
                ctx.closePath();
                //ctx.fill();
                ctx.strokeStyle = 'yellow';
                ctx.stroke();
            }
        }
        /* 5ta zona(medio) */
        if (seccion==5){
            if(ctx){
                ctx.fillStyle = color_line;
                ctx.beginPath();
                ctx.moveTo(cua+inicio_x,cua+inicio_y);
                ctx.lineTo(ter+inicio_x,cua+inicio_y);
                ctx.lineTo(ter+inicio_x,ter+inicio_y);
                ctx.lineTo(cua+inicio_x,ter+inicio_y);
                ctx.closePath();
                //ctx.fill();
                ctx.strokeStyle = 'yellow';
                ctx.stroke();
            }
        }
    }
    // Funcion para sombrear diente completo
    function marcar_diente(contexto, num_diente, color_pas){
        var ctx = contexto;
        // Definiendo puntos de dibujo
        med = medida;
        num_diente = num_diente - 1;
        color_line = color_pas;
        if (num_diente<16){
            inicio_y = 20;
        }
        else{
            num_diente = num_diente - 16;
            inicio_y = med + 100;
        }
        //alert(num_diente);
        inicio_x = (num_diente*med) + (separacion_x*num_diente) + separacion_x;

        ctx.fillStyle = color_line;
        ctx.beginPath();
        ctx.moveTo(inicio_x,inicio_y);
        ctx.lineTo(inicio_x+40,inicio_y);
        ctx.lineTo(inicio_x+40,inicio_y+40);
        ctx.lineTo(inicio_x,inicio_y+40);
        ctx.closePath();
        ctx.strokeStyle = color_line;
        ctx.stroke();
    }
    // Funcion para sombrear diente completo
    function marcar_extraccion(contexto, num_diente, color_pas){
        var ctx = contexto;
        // Definiendo puntos de dibujo
        med = medida;
        num_diente = num_diente - 1;
        color_line = color_pas;
        if (num_diente<16){
            inicio_y = 20;
        }
        else{
            num_diente = num_diente - 16;
            inicio_y = med + 100;
        }
        //alert(num_diente);
        inicio_x = (num_diente*med) + (separacion_x*num_diente) + separacion_x;

        ctx.fillStyle = color_line;
        ctx.beginPath();
        ctx.lineWidth = 3;
        ctx.moveTo(inicio_x,inicio_y);
        ctx.lineTo(inicio_x+40,inicio_y+40);
        ctx.moveTo(inicio_x+40,inicio_y);
        ctx.lineTo(inicio_x,inicio_y+40);
        ctx.stroke();
        ctx.lineWidth = 1;
    }
    // Funcion para marcar puente
    function marcar_puente(contexto, dient_1, dient_2, color_pas){
        var ctx = contexto;
        // Definiendo puntos de dibujo
        med = medida;
        num_diente1 = dient_1 - 1;
        num_diente2 = dient_2 - 1;
        color_line = color_pas;
        if (num_diente1<16){
            inicio_y = 80;
        }
        else{
            num_diente1 = num_diente1 - 16;
            num_diente2 = num_diente2 - 16;
            inicio_y = med + 160;
        }
        //alert(num_diente);
        inicio_x = (num_diente1*med) + (separacion_x*num_diente1) + separacion_x + (med/2);
        fin_x = (num_diente2*med) + (separacion_x*num_diente2) + separacion_x + (med/2);
        ctx.fillStyle = color_line;
        ctx.beginPath();
        ctx.lineWidth = 4;
        ctx.moveTo(inicio_x,inicio_y);
        ctx.lineTo(fin_x,inicio_y);
        //ctx.moveTo(inicio_x+40,inicio_y);
        //ctx.lineTo(inicio_x,inicio_y+40);
        ctx.stroke();
        ctx.lineWidth = 1;
    }
    // Funcion para marcar puente
    function borrar_diente(contexto, num_diente){
        ctx = contexto;
        // Definiendo puntos de dibujo
        med = medida;
        num_diente = num_diente - 1;
        if (num_diente<16){
            inicio_y = 20;
        }
        else{
            num_diente = num_diente - 16;
            inicio_y = med + 100;
        }
        //alert(num_diente);
        inicio_x = (num_diente*med) + (separacion_x*num_diente) + separacion_x;
        ctx.clearRect(inicio_x, inicio_y, med, med);
    }

    // Valores iniciales
    var canvas = document.getElementById('myCanvas');
    var context = canvas.getContext('2d');
    //
    var layer2 = document.getElementById("myCanvas2");
    var ctx2 = layer2.getContext("2d");
    //
    var layer3 = document.getElementById("myCanvas3");
    var ctx3 = layer3.getContext("2d");
    //
    var layer4 = document.getElementById("myCanvas4");
    var ctx4 = layer4.getContext("2d");
    //
    var color_line = 'black';
    var medida = 40;
    var separacion_x = 10;
    var separacion_y = 10;
    var iniciar_x = 0;
    var iniciar_y = 20;
    //Dientes para el puente
    var diente1=0;
    var diente2=0;
    // 1 - 16 dientes
    for (x=0; x<16; x++){
        iniciar_x =  (x*medida) + (separacion_x*x) + separacion_x;
        dibuja_contorno(context, iniciar_x, iniciar_y, medida, separacion_x, 10);
        /* Numero de diente */
        context.font = '10pt Calibri';
        context.textAlign = 'center';
        context.fillStyle = 'blue';
        context.fillText(x+1, iniciar_x+(medida/2), (iniciar_y/2)+5);
    }
    // 17 - 32 dientes
    iniciar_x = 0;
    iniciar_y = medida + 100;
    for (x=0; x<16; x++){
        iniciar_x =  (x*medida) + (separacion_x*x) + separacion_x;
        dibuja_contorno(context, iniciar_x, iniciar_y, medida, separacion_x, 10);
        /* Numero de diente */
        context.font = '10pt Calibri';
        context.textAlign = 'center';
        context.fillStyle = 'blue';
        context.fillText(x+17, iniciar_x+(medida/2), (iniciar_y-10)+5);
    }
    //dibuja_seccion(context, 2, 3, 'red');
    //dibuja_seccion(context, 18, 5, 'green');
    //dibuja_seccion(context, 24, 4, 'blue');
    window.onload = function(){
        localStorage.clear();
        click();
    }
    function click(){
        //Añadimos un addEventListener al canvas para reconocer el click
        layer4.addEventListener("click",
        //Una vez se haya clickado se activará la siguiente función
        getPosition
        ,false);
        layer4.addEventListener("mousemove",Marcar,false);
    }
    //canvas.addEventListener("mousedown", getPosition, false);

    function getPosition(event){
        var x = event.x;
        var y = event.y;
        //alert(y);
        //alert(x);
        var canvas = document.getElementById("myCanvas");
        var div_can = document.getElementById("canvasesdiv");
        x -= div_can.offsetLeft;
        y -= div_can.offsetTop;
        //alert(div_can.offsetTop);
        var div=0;
        var color='';
        var accion='';
        seleccion = $("input[name='accion']:checked").val();
        if (seleccion == 'fractura'){
            color = 'red';
            accion = 'seccion';
        }
        else if(seleccion=='restauracion'){
            color = 'blue';
            accion = 'seccion';
        }
        else if(seleccion=='extraccion'){
            color = 'black';
            accion = 'marcar';
        }
        else if(seleccion=='puente'){
            accion = 'puente';
        }
        else if(seleccion=='borrar'){
            accion = 'borrar';
        }
        //alert(y);
        diente = 0;
        seccion = 0;
        if (y>=20 && y<=60){
            //alert(x);
            if (x>=10 && x<=50){
                diente = 1;
            }
            else if (x>=60 && x<=800){
                div =  parseInt(x/50,10);
                ini = (div*40) + (10*div) + 10;
                fin = ini + 40;
                if (x>=ini && x<=fin){
                    diente=div+1;
                }
            }
        }
        else if(y>=140 && y<=180){
            if (x>=10 && x<=50){
                diente = 17;
            }
            else if (x>=60 && x<=800){
                div =  parseInt(x/50,10);
                ini = (div*40) + (10*div) + 10;
                fin = ini + 40;
                if (x>=ini && x<=fin){
                    diente=div+17;
                }
            }
        }
        if (diente){
            //alert(diente);
            if(accion == 'seccion'){
                x = x-((div*40) + (10*div) + 10);
                y = y-20;
                if (diente>16){
                    y = y-120;
                }
                //alert(y);
                if (y>0 && y<10 && x>y && y<40-x){
                    seccion = 1;
                }else if (x>30 && x<40 && y<x && 40-x<y){
                    seccion = 2;
                }else if (y>30 && y<40 && x<y && x>40-y){
                    seccion = 3;
                }else if (x>0 && x<10 && y>x && x<40-y){
                    seccion = 4;
                }else if (x>10 && x<30 && y>10 && y<30){
                    seccion = 5;
                }
            }else if(accion == 'marcar'){
                cod=diente+'-0-'+'3';
                if(cod && !localStorage.getItem(cod)){
                    new_array=[diente, 0, 3, Date.now(), 0];
                    guardar = new_array.toLocaleString();
                    localStorage.setItem(cod, guardar);
                    marcar_extraccion(ctx2, diente, 'black')
                }else{
                    alert("Ya fue marcado");
                }
            }else if(accion == 'puente'){
                if (diente1==0){
                    marcar_diente(ctx4, diente, 'red');
                    diente1=diente;
                }else if(diente2==0){
                    diente2=diente;
                    menor=0;
                    mayor=0;
                    if(diente1>diente2){
                        mayor=diente1;
                        menor=diente2;
                    }else{
                        mayor=diente2;
                        menor=diente1
                    }
                    diente1=menor;
                    diente2=mayor;
                    if ((diente1<17 && diente2<17 && diente1!=diente2) || (diente1>17 && diente2>17 && diente1!=diente2)){
                        marcar_diente(ctx4, diente, 'red');
                        ctx4.clearRect(0, 0, 810, 70);
                        ctx4.clearRect(0, 135, 810, 70);
                        cod=diente1+'-0-4-'+diente2;
                        if(cod && !localStorage.getItem(cod)){
                            new_array=[diente1, 0, 4, Date.now(), diente2];
                            guardar = new_array.toLocaleString();
                            localStorage.setItem(cod, guardar);
                        }else{
                            alert("Ya fue marcado");
                        }
                        marcar_puente(ctx4, diente1, diente2, 'red');
                    }else{
                        ctx4.clearRect(0, 0, 810, 70);
                        ctx4.clearRect(0, 135, 810, 70);
                    }
                    
                    diente1=0;
                    diente2=0;
                }
            }else if(accion == 'borrar'){
                borrar_diente(ctx2, diente);
                //cargar el ultimo pintado
                seccion_chk = $("input[name='seccion']:checked").val();
                if (seccion_chk=='seccion'){
                    x = x-((div*40) + (10*div) + 10);
                    y = y-20;
                    if (diente>16){
                        y = y-120;
                    }
                    seccion_b=ubica_seccion(x,y);
                    if (seccion_b){
                        ultimo = '';
                        key_cod='';
                        for(var i=0; i < localStorage.length; i++){
                            var key_name = localStorage.key(i);
                            item = localStorage.getItem(key_name);
                            item = item.split(',');
                            diente_comp = parseInt(item[0],10);
                            seccion_comp = parseInt(item[1],10);
                            accion_comp = parseInt(item[2],10);
                            if(diente_comp==diente && seccion_b==seccion_comp && (accion_comp==1 || accion_comp==2)){
                                if(ultimo==''){
                                    ultimo=item;
                                    key_cod=key_name;
                                }
                                else{
                                    fecha_ult = parseInt(item[3],10);
                                    if(ultimo[3]<fecha_ult){
                                        ultimo=item;
                                        key_cod=key_name;
                                    }
                                }
                            }
                        }
                        if(key_cod!=''){
                            //console.log(key_cod);
                            localStorage.removeItem(key_cod);
                        }
                    }

                }else if(seccion_chk=='diente'){
                    ultimo = '';
                    key_cod='';
                    for(var i=0; i < localStorage.length; i++){
                        var key_name = localStorage.key(i);
                        item = localStorage.getItem(key_name);
                        item = item.split(',');
                        diente_comp = parseInt(item[0],10);
                        accion_comp = parseInt(item[2],10);
                        if(diente_comp==diente && accion_comp==3 ){
                            if(ultimo==''){
                                ultimo=item;
                                key_cod=key_name;
                            }
                            else{
                                fecha_ult = parseInt(item[3],10);
                                if(ultimo[3]<fecha_ult){
                                    ultimo=item;
                                    key_cod=key_name;
                                }
                            }
                        }
                    }
                    if(key_cod!=''){
                        //console.log(key_cod);
                        localStorage.removeItem(key_cod);
                    }
                }
                //alert('a');
                pinta_datos();
            }
        }
        //alert(diente);
        if (seccion && color!=''){
            //alert(color);
            
            //alert(seccion);
            //[numero_diente, seccion, accion, fecha, diente2]
            if (color=='red'){
                cod=diente+'-'+seccion+'-'+'1';
                accion_g = 1;
            }else if (color=='blue'){
                cod=diente+'-'+seccion+'-'+'2';
                accion_g = 2;
            };
            if(cod && !localStorage.getItem(cod)){
                new_array=[diente, seccion, accion_g, Date.now(), 0];
                guardar = new_array.toLocaleString();
                localStorage.setItem(cod, guardar);
                dibuja_seccion(ctx2, diente, seccion, color);
            }
            else{
                alert("ya fue marcado");
            }
            
        }
        if ( 'borrar' == $("input[name='accion']:checked").val()){
            //alert("x-> "+x+" y-> "+y);
            //ctx4.clearRect(0, 0, 810, 300);
            
            if (x>=30 && x<=780 && ((y>78 && y<82) || (y>198 && y<202))){
                //alert(x);
                div =  parseInt(x/50,10);
                //alert(div);
                ultimo = '';
                key_cod='';
                for(var i=0; i < localStorage.length; i++){
                    var key_name = localStorage.key(i);
                    item = localStorage.getItem(key_name);
                    item = item.split(',');
                    diente1_comp = parseInt(item[0],10);
                    diente2_comp = parseInt(item[4],10);
                    accion_comp = parseInt(item[2],10);
                    if(accion_comp==4){
                        if (diente1_comp>16){
                            diente1_comp = diente1_comp - 17;
                            diente2_comp = diente2_comp - 17;
                        }else{
                            diente1_comp = diente1_comp - 1;
                            diente2_comp = diente2_comp - 1;
                        }
                        inicio_x = (diente1_comp*40) + (10*diente1_comp) + 10 + 20;
                        fin_X = (diente2_comp*40) + (10*diente2_comp) + 10 + 20;
                        if (x>=inicio_x && x<=fin_x){
                            if(ultimo==''){
                                ultimo=item;
                                key_cod=key_name;
                            }
                            else{
                                fecha_ult = parseInt(item[3],10);
                                if(ultimo[3]<fecha_ult){
                                    ultimo=item;
                                    key_cod=key_name;
                                }
                            }
                        }
                        
                    }
                }
                if(key_cod!=''){
                    console.log(key_cod);
                    if(parseInt(ultimo[0],10)<16){
                        seccion_p=1;
                        ctx4.clearRect(0, 0, 810, 130);
                    }
                    else{
                        ctx4.clearRect(0, 130, 810, 150);
                        seccion_p=2;
                    }
                    localStorage.removeItem(key_cod);
                    pinta_puentes(seccion_p);
                }
            }
            
        }
        
    }

        //dibuja_seccion(x, y, 5, 5);
        //dibuja_seccion(context, num_diente, seccion, color)
    
    
    function Marcar(event){
        var x = event.x;
        var y = event.y;
        var canvas2 = document.getElementById("myCanvas2");
        var div_can = document.getElementById("canvasesdiv");
        x -= div_can.offsetLeft;
        y -= div_can.offsetTop;
        //alert(x);
        diente=0;
        seccion=0;
        var div=0;
        
        if (y>=20 && y<=60){
            //alert(x);
            if (x>=10 && x<=50){
                diente = 1;
                //alert("1");
            }
            else if (x>=60 && x<=800){
                div =  parseInt(x/50,10);
                ini = (div*40) + (10*div) + 10;
                fin = ini + 40;
                if (x>=ini && x<=fin){
                    diente=div+1;
                }
            }
        }
        else if(y>=140 && y<=180){
            if (x>=10 && x<=50){
                diente = 17;
            }
            else if (x>=60 && x<=800){
                div =  parseInt(x/50,10);
                ini = (div*40) + (10*div) + 10;
                fin = ini + 40;
                if (x>=ini && x<=fin){
                    diente=div+17;
                }
            }
        }
        //alert(diente);
        if (diente) {
            accion = $("input[name='accion']:checked").val();
            var seleccion = '';
            if (accion == 'fractura'){
                seleccion = 'seccion';
            }
            else if(accion=='restauracion'){
                seleccion = 'seccion';
            }
            else if(accion=='extraccion'){
                seleccion = 'diente';
            }
            else if(accion=='puente'){
                seleccion = 'diente';
            }
            else if(accion=='borrar'){
                seccion_chk = $("input[name='seccion']:checked").val();
                if (seccion_chk=='diente'){
                    seleccion = 'diente';
                }
                else{
                    seleccion = 'seccion';
                }
            }
            if (seleccion=='seccion'){
                x = x-((div*40) + (10*div) + 10);
                y = y-20;
                if (diente>16){
                    y = y-120;
                }
                //alert(y);
                /*if (y>=x && y<=39){}*/
                // Ubicar la seccion clickeada
                if (y>0 && y<10 && x>y && y<40-x){
                    seccion = 1;
                }else if (x>30 && x<40 && y<x && 40-x<y){
                    seccion = 2;
                }else if (y>30 && y<40 && x<y && x>40-y){
                    seccion = 3;
                }else if (x>0 && x<10 && y>x && x<40-y){
                    seccion = 4;
                }else if (x>10 && x<30 && y>10 && y<30){
                    seccion = 5;
                }
                //Comprobacion de si eta en una seccion
                if (seccion){
                    //alert(seccion);
                    color = 'yellow';
                    ctx3.clearRect(0, 0, 810, 300);
                    marcar_seccion(ctx3, diente, seccion, color);
                    //alert(seccion);
                }else{
                    //ctx2.fillStyle = "white";
                    //ctx2.fillRect(0, 0, 810, 300);
                    ctx3.clearRect(0, 0, 810, 300);
                }
            }
            else if(seleccion=='diente'){
                ctx3.clearRect(0, 0, 810, 300);
                marcar_diente(ctx3, diente, 'yellow');
            }
        }else{
            ctx3.clearRect(0, 0, 810, 300);
        }
        
        //dibuja_contorno(canvas, inicio_x, inicio_y, med, separacion_x, separacion_y)
    }
    function pinta_datos(){
        array_local = [];
        for(var i=0; i < localStorage.length; i++){
            var key_name = localStorage.key(i);
            array_local[i]=localStorage.getItem(key_name).split(',');
        }
        //console.log(array_local);
        array_local.sort(function(a,b){
            return a[3] > b[3]; // orden ascendente por las fechas;
        });
        //console.log(array_local);
        for(var i=0; i < array_local.length; i++){
            item = array_local[i];
            if(parseInt(item[0],10)==diente){
                acc = parseInt(item[2],10);
                //console.log(acc);
                if (acc==1){
                    color='red';
                    dibuja_seccion(ctx2, item[0], item[1], color);
                }else if (acc==2){
                    color='blue';
                    dibuja_seccion(ctx2, item[0], item[1], color);
                }else if (acc==3){
                    marcar_extraccion(ctx2, item[0], 'black');
                }
            }
        }
    }
    function pinta_puentes(seccion_p){
        array_local = [];
        for(var i=0; i < localStorage.length; i++){
            var key_name = localStorage.key(i);
            array_local[i]=localStorage.getItem(key_name).split(',');
        }
        //console.log(array_local);
        array_local.sort(function(a,b){
            return a[3] > b[3]; // orden ascendente por las fechas;
        });
        //console.log(array_local);
        for(var i=0; i < array_local.length; i++){
            item = array_local[i];
            acc = parseInt(item[2],10);
            //console.log(acc);
            if (acc==4){
                color_pas = 'red';
                if (seccion_p==1){
                    if(parseInt(item[0],10)<17){
                        marcar_puente(ctx4, item[0], item[4], color_pas);
                    }
                }
                else{
                    if(parseInt(item[0],10)>16){
                        marcar_puente(ctx4, item[0], item[4], color_pas);
                    }
                }
                //dibuja_seccion(ctx2, item[0], item[1], color);
            }
            
        }
    }
    function ubica_seccion(X,Y){
        y=Y;
        x=X;
        devolver_seccion=0;
        if (y>0 && y<10 && x>y && y<40-x){
            devolver_seccion = 1;
        }else if (x>30 && x<40 && y<x && 40-x<y){
            devolver_seccion = 2;
        }else if (y>30 && y<40 && x<y && x>40-y){
            devolver_seccion = 3;
        }else if (x>0 && x<10 && y>x && x<40-y){
            devolver_seccion = 4;
        }else if (x>10 && x<30 && y>10 && y<30){
            devolver_seccion = 5;
        }
        return devolver_seccion;
    }
</script>
@endsection