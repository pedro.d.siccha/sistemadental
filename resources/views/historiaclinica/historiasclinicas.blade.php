@extends('layouts.app')
@section('nombrePagina')
 Mis Historias Clinicas
@endsection
@section('contenido')
<div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
        <div class="x_title">
        <h2>Lista de Historias Clinicas <small>Pacientes</small></h2>
        <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
            <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
            <ul class="dropdown-menu" role="menu">
                <li><a href="#">Settings 1</a>
                </li>
                <li><a href="#">Settings 2</a>
                </li>
            </ul>
            </li>
            <li><a class="close-link"><i class="fa fa-close"></i></a>
            </li>
        </ul>
        <div class="clearfix"></div>
        </div>
        <div class="x_content">
        <table id="datatable" class="table table-striped table-bordered">
            <thead>
            <tr>
                <th>Estado</th>
                <th>Fecha</th>
                <th>Num Historia</th>
                <th>Paciente</th>
                <th>DNI</th>
                <th>Edad</th>
                <th>Correo</th>
                <th>Teléfono</th>
                <th>Gestion</th>
            </tr>
            </thead>


            <tbody>
                @foreach ($historiaClinica as $hc)
                <tr>
                    <td>{{ $hc->estado }}</td>
                    <td>{{ $hc->feccreacion }}</td>
                    <td>{{ $hc->historiaclinica_id }}</td>
                    <td>{{ $hc->nombre }} {{ $hc->apellido }}</td>
                    <td>{{ $hc->dni }}</td>
                    <td>{{ $hc->edad }} años</td>
                    <td>{{ $hc->correo }}</td>
                    <td>{{ $hc->telefono }}</td>
                    <td><a href=" {{ route('histclinicapaciente', [ $hc->historiaclinica_id ]) }} " type="button" class="btn btn-primary btn-xs" data-toggle="tooltip" data-placement="top" title="Ver Historia Clinica"><i class="fa fa-calendar-o"></i></a><button type="button" class="btn btn-warning btn-xs" data-toggle="tooltip" data-placement="top" title="Dar de Baja"><i class="fa fa-minus-circle"></i></button><button type="button" class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="top" title="Eliminar Historia Clinica"><i class="fa fa-trash"></i></button></td>
                </tr>
                @endforeach
            </tbody>
        </table>
        </div>
    </div>
</div>
@endsection