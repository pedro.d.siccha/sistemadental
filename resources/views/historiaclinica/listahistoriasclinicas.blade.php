@extends('layouts.app')
@section('nombrePagina')
 Historias Clinicas   
@endsection
@section('contenido')
<div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
        <div class="x_title">
        <h2>Lista de Historias Clinicas <small>Pacientes</small></h2>
        <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
            <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
            <ul class="dropdown-menu" role="menu">
                <li><a href="#">Settings 1</a>
                </li>
                <li><a href="#">Settings 2</a>
                </li>
            </ul>
            </li>
            <li><a class="close-link"><i class="fa fa-close"></i></a>
            </li>
        </ul>
        <div class="clearfix"></div>
        </div>
        <div class="x_content">
        <table id="datatable" class="table table-striped table-bordered">
            <thead>
            <tr>
                <th>Estado</th>
                <th>Fecha</th>
                <th>Num Historia</th>
                <th>Paciente</th>
                <th>DNI</th>
                <th>Edad</th>
                <th>Especialidad</th>
                <th>Medico Asignado</th>
                <th>Gestion</th>
            </tr>
            </thead>


            <tbody>
            <tr>
                <td>Activo</td>
                <td>16/11/2019</td>
                <td>00001</td>
                <td>Angelica Ramos</td>
                <td>72589648</td>
                <td>28</td>
                <td>Odontologia General</td>
                <td>Bruno Nash</td>
                <td><button type="button" class="btn btn-primary btn-xs" data-toggle="tooltip" data-placement="top" title="Ver Historia Clinica"><i class="fa fa-calendar-o"></i></button><button type="button" class="btn btn-warning btn-xs" data-toggle="tooltip" data-placement="top" title="Dar de Baja"><i class="fa fa-minus-circle"></i></button><button type="button" class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="top" title="Eliminar Historia Clinica"><i class="fa fa-trash"></i></button></td>
            </tr>
            <tr>
                <td>Activo</td>
                <td>16/11/2019</td>
                <td>00002</td>
                <td>Manuel Cubillas</td>
                <td>40856487</td>
                <td>31</td>
                <td>Odontologia General</td>
                <td>Carlos Benitez</td>
                <td><button type="button" class="btn btn-primary btn-xs" data-toggle="tooltip" data-placement="top" title="Ver Historia Clinica"><i class="fa fa-calendar-o"></i></button><button type="button" class="btn btn-warning btn-xs" data-toggle="tooltip" data-placement="top" title="Dar de Baja"><i class="fa fa-minus-circle"></i></button><button type="button" class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="top" title="Eliminar Historia Clinica"><i class="fa fa-trash"></i></button></td>
            </tr>
            </tbody>
        </table>
        </div>
    </div>
</div>
@endsection