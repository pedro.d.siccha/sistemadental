@extends('layouts.app')
@section('nombrePagina')
 Citas Diarias
@endsection
@section('contenido')
<div role="main">
    <div class="">
        <div class="page-title">
        <div class="title_left">
            <h3>Generar Citas</h3>
        </div>
        </div>

        <div class="clearfix"></div>

        <div class="row">
        <div class="col-md-12">
            <div class="x_panel">
            <div class="x_title">
                <h2>Calendario de Citas</h2>
                <ul class="nav navbar-right panel_toolbox">
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                    <ul class="dropdown-menu" role="menu">
                    <li><a href="#">Settings 1</a>
                    </li>
                    <li><a href="#">Settings 2</a>
                    </li>
                    </ul>
                </li>
                <li><a class="close-link"><i class="fa fa-close"></i></a>
                </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content" id="calenda">

               <!-- <div id='calendar'></div> -->
                <div id='calendario'></div>

            </div>
            </div>
        </div>
        </div>
    </div>
</div>

<!-- Modal Crear Cita -->
<div id="NuevaCita" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
            </button>
            <h4 class="modal-title" id="myModalLabel">Generar Nueva Cita</h4>
        </div>
        <div class="modal-body row">
            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                <label class="control-label col-md-3 col-sm-3 col-xs-6">Médico</label>
                <input type="text" class="form-control has-feedback-left" id="medico" placeholder="Buscar con DNI o Nombre" onclick="mostrarMedicos()" readonly>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                <label class="control-label col-md-3 col-sm-3 col-xs-6">Especialidad</label>
                    <select id="especialidad_id" class="form-control" required> 
                        <option>Seleccionar..</option>
                        @foreach ($especialidad as $e)
                        <option value="{{ $e->id }}">{{ $e->nombre }}</option>
                        @endforeach
                    </select>    
            </div>
            <div class="col-md-3 col-sm-3 col-xs-12 form-group has-feedback">
                <label class="control-label col-md-3 col-sm-3 col-xs-6">Paciente</label>
                <input type="text" class="form-control has-feedback-left" id="dni" placeholder="Ingrese DNI" onkeyup="buscarPaciente(event)">
                <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
            </div>
            <div class="col-md-9 col-sm-9 col-xs-12 form-group has-feedback">
                <label class="control-label col-md-3 col-sm-3 col-xs-6">*</label>
                <input type="text" class="form-control has-feedback-left" id="paciente" placeholder="Paciente" readonly>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
                <label class="control-label col-md-6 col-sm-6 col-xs-6">DNI Acompañante</label>
                <input type="text" class="form-control has-feedback-left" id="dniAcom" placeholder="Ingrese DNI del Acompañante" onkeyup="buscarAcompaniante(event)">
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
                <label class="control-label col-md-3 col-sm-3 col-xs-6">Acompañante</label>
                <input type="text" class="form-control has-feedback-left" id="nomAcom">
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
                <label class="control-label col-md-6 col-sm-6 col-xs-6">Parentezco</label>
                <input type="text" class="form-control has-feedback-left" id="parenAcom">
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
                <label class="control-label col-md-3 col-sm-3 col-xs-6">Fecha</label>
                <input type="date" class="form-control has-feedback-left" id="fechaCit">
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
                <label class="control-label col-md-3 col-sm-3 col-xs-6">Inicio</label>
                <input type="time" class="form-control has-feedback-left" id="inicioCit">
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
                <label class="control-label col-md-3 col-sm-3 col-xs-6">Fin</label>
                <input type="time" class="form-control has-feedback-left" id="finCit">
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                <label class="control-label col-md-3 col-sm-3 col-xs-6">Comentario</label>
                <textarea class="form-control" rows="3" cols="12" id="comentarioCit"></textarea>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            <button type="button" class="btn btn-primary" onclick="crearCita()">Crear Cita</button>
        </div>

        </div>
    </div>
</div>

<!-- Fin Modal Crear Cita -->

<!-- Modal Lista de Médicos -->

<div id="listaMedicos" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title" id="myModalLabel2">Lista de Médicos</h4>
        </div>
        <div class="modal-body">    
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Medico</th>
                        <th>DNI</th>
                        <th>Especialidad</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($medico as $m)
                    <tr>
                        <td><button type="button" class="btn btn-default" onclick="seleccionarMedico('{{ $m->medico_id }}')"><i class="fa fa-check-circle"></i></button></td>
                        <td>{{ $m->nombre }} {{ $m->apellido }}</td>
                        <td>{{ $m->dni }}</td>
                        <td>{{ $m->especialidad }}</td>
                    </tr>    
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default antoclose2" data-dismiss="modal">Cancelar</button>
        </div>
        </div>
    </div>
</div>
<!-- Fin Modal Lista de Médicos -->

<!-- Modal Crear Paciente -->
<div id="crearPaciente" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
            </button>
            <h4 class="modal-title" id="myModalLabel">Crear Nuevo Paciente</h4>
        </div>
        <div class="modal-body row">
            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                <label class="control-label col-md-3 col-sm-3 col-xs-6">DNI</label>
                <input type="text" class="form-control has-feedback-left" id="dniNuevo" placeholder="Nuevo DNI" readonly>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                <label class="control-label col-md-3 col-sm-3 col-xs-6">Nombre</label>
                <input type="text" class="form-control has-feedback-left" id="nombreNuevo" placeholder="Ingrese Nombre">
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                <label class="control-label col-md-3 col-sm-3 col-xs-6">Apellido</label>
                <input type="text" class="form-control has-feedback-left" id="apellidoNuevo" placeholder="Ingrese Apellido">
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                <label class="control-label col-md-3 col-sm-3 col-xs-6">Direccion</label>
                <input type="text" class="form-control has-feedback-left" id="direccionNuevo" placeholder="Ingrese Dirección">
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                <label class="control-label col-md-3 col-sm-3 col-xs-6">Referencia</label>
                <input type="text" class="form-control has-feedback-left" id="referenciaNuevo" placeholder="Ingrese Referencia">
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                <label class="control-label col-md-3 col-sm-3 col-xs-6">Teléfono</label>
                <input type="text" class="form-control has-feedback-left" id="telefonoNuevo" placeholder="Ingresar Teléfono">
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                <label class="control-label col-md-6 col-sm-6 col-xs-6">Fecha de Nacimiento</label>
                <input type="date" class="form-control has-feedback-left" id="fecnacimientoNuevo">
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                <label class="control-label col-md-3 col-sm-3 col-xs-6">Edad</label>
                <input type="text" class="form-control has-feedback-left" id="edadNuevo" placeholder="Ingrese Edad">
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                <label class="control-label col-md-3 col-sm-3 col-xs-6">Género</label>
                <input type="text" class="form-control has-feedback-left" id="generoNuevo" placeholder="Ingresar Género">
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                <label class="control-label col-md-3 col-sm-3 col-xs-6">Correo</label>
                <input type="text" class="form-control has-feedback-left" id="correoNuevo" placeholder="Ingrese Correo">
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            <button type="button" class="btn btn-primary" onclick="guardarPaciente()">Guardar</button>
        </div>

        </div>
    </div>
</div>
<!-- Fin Modal Crear Paciente -->

<!-- Modal Crear Acompañante -->
<div id="crearAcompaniante" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
            </button>
            <h4 class="modal-title" id="myModalLabel">Crear Nuevo Paciente</h4>
        </div>
        <div class="modal-body row">
            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                <label class="control-label col-md-3 col-sm-3 col-xs-6">DNI</label>
                <input type="text" class="form-control has-feedback-left" id="dniAcomNuevo" placeholder="Nuevo DNI" readonly>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                <label class="control-label col-md-3 col-sm-3 col-xs-6">Nombre</label>
                <input type="text" class="form-control has-feedback-left" id="nombreAcomNuevo" placeholder="Ingrese Nombre">
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                <label class="control-label col-md-3 col-sm-3 col-xs-6">Apellido</label>
                <input type="text" class="form-control has-feedback-left" id="apellidoAcomNuevo" placeholder="Ingrese Apellido">
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                <label class="control-label col-md-3 col-sm-3 col-xs-6">Direccion</label>
                <input type="text" class="form-control has-feedback-left" id="direccionAcomNuevo" placeholder="Ingrese Dirección">
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                <label class="control-label col-md-3 col-sm-3 col-xs-6">Referencia</label>
                <input type="text" class="form-control has-feedback-left" id="referenciaAcomNuevo" placeholder="Ingrese Referencia">
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                <label class="control-label col-md-3 col-sm-3 col-xs-6">Teléfono</label>
                <input type="text" class="form-control has-feedback-left" id="telefonoAcomNuevo" placeholder="Ingresar Teléfono">
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                <label class="control-label col-md-6 col-sm-6 col-xs-6">Fecha de Nacimiento</label>
                <input type="date" class="form-control has-feedback-left" id="fecnacimientoAcomNuevo">
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                <label class="control-label col-md-3 col-sm-3 col-xs-6">Edad</label>
                <input type="text" class="form-control has-feedback-left" id="edadAcomNuevo" placeholder="Ingrese Edad">
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                <label class="control-label col-md-3 col-sm-3 col-xs-6">Género</label>
                <input type="text" class="form-control has-feedback-left" id="generoAcomNuevo" placeholder="Ingresar Género">
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                <label class="control-label col-md-3 col-sm-3 col-xs-6">Correo</label>
                <input type="text" class="form-control has-feedback-left" id="correoAcomNuevo" placeholder="Ingrese Correo">
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                <label class="control-label col-md-3 col-sm-3 col-xs-6">Parentezco</label>
                    <select id="parentezco_id" class="form-control" required> 
                        <option>Seleccionar..</option>
                        @foreach ($parentezco as $p)
                        <option value="{{ $p->id }}">{{ $p->nombre }}</option>
                        @endforeach
                    </select>    
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            <button type="button" class="btn btn-primary" onclick="guardarAcompaniante()">Guardar</button>
        </div>

        </div>
    </div>
</div>
<!-- Fin Modal Crear Acompañante -->

<!-- Modal Detalle Cita -->
<div id="modalDetalleCita" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title" id="tituloCita"></h4>
        </div>
        <div class="modal-body">
            <div id="testmodal2" style="padding: 5px 20px;">
            <form id="antoform2" class="form-horizontal calender" role="form">
                <div class="form-group">
                <label class="col-sm-3 control-label">Paciente</label>
                <div class="col-sm-9">
                    <label class="col-sm-12 control-label" id="pacienteCita">Paciente</label>
                </div>
                </div>
                <div class="form-group">
                <label class="col-sm-3 control-label">Descripcion</label>
                <div class="col-sm-9">
                    <label class="col-sm-12 control-label" id="descripcionCita">Descripcion</label>
                </div>
                </div>

            </form>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default antoclose2" data-dismiss="modal">Cancelar</button>
            <button type="button" class="btn btn-primary antosubmit2">Guardar</button>
        </div>
        </div>
    </div>
</div>

<!-- Fin Modal Detalle Cita -->

<div id="CalenderModalNew" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title" id="myModalLabel">NUEVA CITA</h4>
        </div>
        <div class="modal-body">
            <div id="testmodal" style="padding: 5px 20px;">
            <form id="antoform" class="form-horizontal calender" role="form">
                <div class="form-group">
                <label class="col-sm-3 control-label">Médico</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="title" name="title">
                </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Especialidad</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="title" name="title">
                    </div>
                </div>
                <div class="form-group">
                <label class="col-sm-3 control-label">Descripcion</label>
                <div class="col-sm-9">
                    <textarea class="form-control" style="height:55px;" id="descr" name="descr"></textarea>
                </div>
                </div>
            </form>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default antoclose" data-dismiss="modal">Cancelar</button>
            <button type="button" class="btn btn-primary antosubmit">Generar</button>
        </div>
        </div>
    </div>
</div>

<div id="CalenderModalEdit" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title" id="myModalLabel2">EDITAR CITA</h4>
        </div>
        <div class="modal-body">

            <div id="testmodal2" style="padding: 5px 20px;">
            <form id="antoform2" class="form-horizontal calender" role="form">
                <div class="form-group">
                <label class="col-sm-3 control-label">Paciente</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="title2" name="title2">
                </div>
                </div>
                <div class="form-group">
                <label class="col-sm-3 control-label">Medico</label>
                <div class="col-sm-9">
                    <textarea class="form-control" style="height:55px;" id="descr2" name="descr"></textarea>
                </div>
                </div>

            </form>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default antoclose2" data-dismiss="modal">Cancelar</button>
            <button type="button" class="btn btn-primary antosubmit2">Guardar</button>
        </div>
        </div>
    </div>
</div>
  
<div id="fc_create" data-toggle="modal" data-target="#CalenderModalNew"></div>
<div id="fc_edit" data-toggle="modal" data-target="#CalenderModalEdit"></div>
@endsection
@section('script')
    
    <link href="{{asset('../vendors/fullcalendar/dist/fullcalendar.css')}}" rel="stylesheet">
    <link href="{{asset('../vendors/fullcalendar/dist/fullcalendar.print.css')}}" rel="stylesheet" media="print">
    <script src="{{asset('../vendors/fullcalendar/dist/fullcalendar.js')}}"></script>
    <script src="{{asset('../vendors/fullcalendar/dist/es.js')}}"></script>
    <script>
        
        $(document).ready(function(){
            //001 Esta parte del codigo se implementa para poder recoger la ruta exacta del json para los datos del fullcalendar
            var evt = [];
            $.ajax({
                url: '/eventosCalendar',
                type: "GET",
                dataType: "JSON",
                async: false
            }).done(function(r) {
                evt = r;
            })
            //Fin 001

            $('#calendario').fullCalendar({
                locale: 'es',
                header:{
                    left:'prev,next, today',
                    center: 'title',
                    right:'month,agendaWeek,agendaDay'
                },
                dayClick:function(date, jsEvent, view){
                    $('#NuevaCita').modal('show');
                    $('#fechaCit').val(date.format());
                    //$(this).css('background-color','#76A8C8');
                },
                events: evt,

                eventClick:function(calEvent, jsEvent, view){
                    $("#tituloCita").html(calEvent.title);
                    $("#pacienteCita").html(calEvent.title);
                    $("#descripcionCita").html(calEvent.descripcion);
                    $("#modalDetalleCita").modal('show');
                }
                
            });
        });

        function mostrarMedicos() {
            $('#listaMedicos').modal('show');
        }

        function seleccionarMedico(medico_id) {
               
            $.post( "{{ Route('buscarMedico') }}", {medico_id: medico_id, _token:'{{csrf_token()}}'}).done(function(data) {
                $('#listaMedicos').modal('hide');
                $("#medico").val(data.nombreMedico);
                $("#especialidad_id").val(data.idEspecialidad);
                
                });
        }

        function buscarPaciente(e) {
            tecla = (document.all) ? e.keyCode : e.which;

            if (tecla == 13) {
                var dni = $('#dni').val();

                $.post( "{{ Route('buscarPaciente') }}", {dni: dni, _token:'{{csrf_token()}}'}).done(function(data) {

                    if (data.nombrePaciente != 0) {
                        $("#paciente").val(data.nombrePaciente);        
                    }else{
                        //window.open('{{ Route('pacientes') }}');
                        $('#crearPaciente').modal('show');
                        $("#dniNuevo").val(dni);
                    }
                
                
                });
            }
        }

        function guardarPaciente() {

            var dni = $('#dniNuevo').val();
            var nombre = $('#nombreNuevo').val();
            var apellido = $('#apellidoNuevo').val();
            var direccion = $('#direccionNuevo').val();
            var referencia = $('#referenciaNuevo').val();
            var telefono = $('#telefonoNuevo').val();
            var fecnacimiento = $('#fecnacimientoNuevo').val();
            var edad = $('#edadNuevo').val();
            var genero = $('#generoNuevo').val();
            var correo = $('#correoNuevo').val();
               
               $.post( "{{ Route('guardarPaciente') }}", {dni: dni, nombre: nombre, apellido: apellido, direccion: direccion, referencia: referencia, telefono: telefono, fecnacimiento: fecnacimiento, edad: edad, genero: genero, correo: correo, _token:'{{csrf_token()}}'}).done(function(data) {
                   $('#crearPaciente').modal('hide');
                   $("#paciente").val(data.nombrePaciente);
                   
                   });
           }

           function buscarAcompaniante(e) {
            tecla = (document.all) ? e.keyCode : e.which;

            if (tecla == 13) {
                var dni = $('#dniAcom').val();

                $.post( "{{ Route('buscarAcompaniante') }}", {dni: dni, _token:'{{csrf_token()}}'}).done(function(data) {

                    if (data.nombreAcompaniante != 0) {
                        $("#nomAcom").val(data.nombreAcompaniante);
                        $("#parenAcom").val(data.parentezco);
                    }else{
                        //window.open('{{ Route('pacientes') }}');
                        $('#crearAcompaniante').modal('show');
                        $("#dniAcomNuevo").val(dni);
                    }
                
                
                });
            }
        }

        function guardarAcompaniante() {

            var dni = $('#dniAcomNuevo').val();
            var nombre = $('#nombreAcomNuevo').val();
            var apellido = $('#apellidoAcomNuevo').val();
            var direccion = $('#direccionAcomNuevo').val();
            var referencia = $('#referenciaAcomNuevo').val();
            var telefono = $('#telefonoAcomNuevo').val();
            var fecnacimiento = $('#fecnacimientoAcomNuevo').val();
            var edad = $('#edadAcomNuevo').val();
            var genero = $('#generoAcomNuevo').val();
            var correo = $('#correoAcomNuevo').val();
            var parentezco_id = $('#parentezco_id').val();
               
               $.post( "{{ Route('guardarAcompaniante') }}", {dni: dni, nombre: nombre, apellido: apellido, direccion: direccion, referencia: referencia, telefono: telefono, fecnacimiento: fecnacimiento, edad: edad, genero: genero, correo: correo, parentezco_id: parentezco_id, _token:'{{csrf_token()}}'}).done(function(data) {
                   $('#crearAcompaniante').modal('hide');
                   $("#nomAcom").val(data.nombreAcompaniante);
                   $("#parenAcom").val(data.parentezco);
                   
                   });
           }

           function crearCita() {
               var start = $('#fechaCit').val() + " " + $('#inicioCit').val();
               var end = $('#fechaCit').val() + " " + $('#finCit').val();
               var detalle = $('#comentarioCit').val();
               var dniPac = $('#dni').val();
               var dniAcom = $('#dniAcom').val();
               var especialidad_id = $('#especialidad_id').val();
               var medico = $('#medico').val();

               $.post( "{{ Route('crearCita') }}", {start: start, end: end, detalle: detalle, dniPac: dniPac, dniAcom: dniAcom, especialidad_id: especialidad_id, medico: medico, _token:'{{csrf_token()}}'}).done(function(data) {
                        $("#calenda").empty();
                        $("#calenda").html(data.view);        
                        $('#NuevaCita').modal('hide');
                   });
           }
    </script>
@endsection