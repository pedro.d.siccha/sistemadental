<!-- <div id='calendar'></div> -->
<div id='calendario'></div>

    <script>
        $(document).ready(function(){
            //001 Esta parte del codigo se implementa para poder recoger la ruta exacta del json para los datos del fullcalendar
            var evt = [];
            $.ajax({
                url: '/eventosCalendar',
                type: "GET",
                dataType: "JSON",
                async: false
            }).done(function(r) {
                evt = r;
            })
            //Fin 001

            $('#calendario').fullCalendar({
                locale: 'es',
                header:{
                    left:'prev,next, today',
                    center: 'title',
                    right:'month,agendaWeek,agendaDay'
                },
                dayClick:function(date, jsEvent, view){
                    $('#NuevaCita').modal('show');
                    $('#fechaCit').val(date.format());
                    //$(this).css('background-color','#76A8C8');
                },
                events: evt,

                eventClick:function(calEvent, jsEvent, view){
                    $("#tituloCita").html(calEvent.title);
                    $("#pacienteCita").html(calEvent.title);
                    $("#descripcionCita").html(calEvent.descripcion);
                    $("#modalDetalleCita").modal('show');
                }
                
            });
        });
    </script>