@extends('layouts.app')
@section('nombrePagina')
 CAJA   
@endsection
@section('contenido')

<div class="row">

<div class="col-lg-6">
    <div class="ibox ">
        <div class="ibox-title">
            <h5>Caja Salida</h5>
            <div class="ibox-tools">
                <a class="collapse-link">
                    <i class="fa fa-chevron-up"></i>
                </a>
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    <i class="fa fa-wrench"></i>
                </a>
                <a class="close-link">
                    <i class="fa fa-times"></i>
                </a>
            </div>
        </div>
        <div class="ibox-content" id="divTipoPrestamo">
            <table class="footable table table-stripped toggle-arrow-tiny">
                <thead>
                <tr>
                    <th data-toggle="true">Código</th>
                    <th>Concepto</th>
                    <th>Fecha de Transcaccion</th>
                    <th>Monto</th>
                    <th>Usuario</th>
                </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>001-122</td>
                        <td>Pago General</td>
                        <td>20/11/2019</td>
                        <td>S/.50.00</td>
                        <td>Administrador</td>
                    </tr>    
                </tbody>
                <tfoot>
                <tr>
                    <td colspan="5">
                        <ul class="pagination float-right"></ul>
                    </td>
                </tr>
                </tfoot>
            </table>

        </div>
    </div>
</div>

<div class="col-lg-6">
    <div class="ibox ">
        <div class="ibox-title">
            <h5>Caja Entrada</h5>
            <div class="ibox-tools">
                <a class="collapse-link">
                    <i class="fa fa-chevron-up"></i>
                </a>
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    <i class="fa fa-wrench"></i>
                </a>
                <a class="close-link">
                    <i class="fa fa-times"></i>
                </a>
            </div>
        </div>
        <div class="ibox-content" id="divTipoPrestamo">
            <table class="footable table table-stripped toggle-arrow-tiny">
                <thead>
                    <tr>
                        <th data-toggle="true">Código</th>
                        <th>Concepto</th>
                        <th>Fecha de Transcaccion</th>
                        <th>Monto</th>
                        <th>Usuario</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>001-123</td>
                        <td>Pago 01</td>
                        <td>20/11/2019</td>
                        <td>S/. 200.00</td>
                        <td>Administrador</td>
                    </tr>    
                </tbody>
                <tfoot>
                <tr>
                    <td colspan="5">
                        <ul class="pagination float-right"></ul>
                    </td>
                </tr>
                </tfoot>
            </table>

        </div>
    </div>
</div>

<div class="col-lg-12">
    <div class="ibox ">
        <div class="ibox-title">
            <h5>Balance de Caja</h5>
            <div class="ibox-tools">
                <a class="collapse-link">
                    <i class="fa fa-chevron-up"></i>
                </a>
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    <i class="fa fa-wrench"></i>
                </a>
                <a class="close-link">
                    <i class="fa fa-times"></i>
                </a>
            </div>
        </div>
        <div class="ibox-content" id="divTipoPrestamo">
            <table class="footable table table-stripped toggle-arrow-tiny">
                <thead>
                    <tr>
                        <th data-toggle="true">Código</th>
                        <th>Mes</th>
                        <th>Caja Salida</th>
                        <th>Caja Entrada</th>
                        <th>Balance</th>
                    </tr>
                </thead>
                <tbody>
                    
                    <tr>
                        <td>01</td>
                        <td>Octubre</td>
                        <td>S/. 200.00</td>
                        <td>S/. 0.00</td>
                        <td class="red-bg">$/. 200.00  <i class="fa fa-angle-double-down fa-1x"></i></td>
                    </tr>
                </tbody>
                <tfoot>
                <tr>
                    <td colspan="5">
                        <ul class="pagination float-right"></ul>
                    </td>
                </tr>
                </tfoot>
            </table>

        </div>
    </div>
</div>
</div>
<!-- Abrir Caja -->
<div class="modal inmodal fade" id="caja" tabindex="-1" role="dialog"  aria-hidden="true">
<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <h4 class="modal-title">Abrir Caja</h4>
            <small class="font-bold">Caja {{ date('d/m/Y') }}:</small>
        </div>
        <div class="modal-body">

            <table class="table m-b-xs">
                <tbody>
                <tr hidden>
                    <td>
                        <strong>idAlmacen</strong>
                    </td>
                    <td>
                        <span id="idAlmacenNC">idAlmacen</span>
                    </td>

                </tr>
                <tr>
                    <td>
                        <strong>Periodo Caja Anterior</strong>
                    </td>
                    <td>
                        <span id="periodCaja">almacen</span>
                    </td>

                </tr>
                <tr>
                    <td>
                        <strong>Monto De Cierre Anterior</strong>
                    </td>
                    <td id="cbStand">
                        <span id="montoCierre">almacen</span>
                    </td>
                </tr>
                </tbody>
            </table>
            <div class="col-sm-12"><p style='text-align:left;'>Monto Caja:</p>
                <input style="font-size: large;" type="text" class="form-control text-success" id="montoInicial" placeholder="S/. 0.00">
            </div>
        </div>

        <div class="modal-footer">
            <button type="button" class="btn btn-outline btn-success dim" data-dismiss="modal" onclick="abrirCaja()"><i class="fa fa-money"></i> Abrir Caja</button>
        </div>
    </div>
</div>
</div>

<!-- Abrir Caja -->
<div class="modal inmodal fade" id="cerrarCaja" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Cerrar Caja</h4>
                <small class="font-bold">Caja {{ date('d/m/Y') }}:</small>
            </div>
            <div class="modal-body">

                <table class="table m-b-xs">
                    <tbody>
                    <tr>
                        <td>
                            <strong>Periodo Caja Anterior</strong>
                        </td>
                        <td>
                            <span id="periodCajaC">almacen</span>
                        </td>
    
                    </tr>
                    <tr>
                        <td>
                            <strong>Monto De Cierre Anterior</strong>
                        </td>
                        <td id="cbStand">
                            <span id="montoCierreC">almacen</span>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-outline btn-success dim" data-dismiss="modal" onclick="abrirCaja()"><i class="fa fa-money"></i> Abrir Caja</button>
            </div>
        </div>
    </div>
</div>
@endsection