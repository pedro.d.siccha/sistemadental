@extends('layouts.app')
@section('nombrePagina')
 VENTAS
@endsection
@section('contenido')
<div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
        <div class="x_title">
        <h2>Ventas </h2>
        <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
            <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
            <ul class="dropdown-menu" role="menu">
                <li><a href="#">Settings 1</a>
                </li>
                <li><a href="#">Settings 2</a>
                </li>
            </ul>
            </li>
            <li><a class="close-link"><i class="fa fa-close"></i></a>
            </li>
        </ul>
        <div class="clearfix"></div>
        </div>
        <div class="x_content">
        <table id="datatable" class="table table-striped table-bordered">
            <thead>
            <tr>
                <th>Fecha</th>
                <th>Tipo de Comprobante</th>
                <th>N° Doc</th>
                <th>Paciente</th>
                <th>Total</th>
                <th>Gestion</th>
            </tr>
            </thead>


            <tbody>
            <tr>
                <td>20/11/2019</td>
                <td>Boleta</td>
                <td>001-123</td>
                <td>Jesus Romero</td>
                <td>S/. 200.00</td>
                <td><button type="button" class="btn btn-warning btn-xs" data-toggle="tooltip" data-placement="top" title="Dar de Baja"><i class="fa fa-minus-circle"></i></button><button type="button" class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="top" title="Eliminar Historia Clinica"><i class="fa fa-trash"></i></button></td>
            </tr>
            </tbody>
        </table>
        </div>
    </div>
</div>
@endsection