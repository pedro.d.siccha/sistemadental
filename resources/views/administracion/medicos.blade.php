@extends('layouts.app')
@section('nombrePagina')
 MEDICOS
@endsection
@section('contenido')
<div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
        <div class="x_title">
        <h2>Lista de Medicos <button type="button" class="btn btn-default btn-xs" data-toggle="tooltip" data-placement="top" title="Registrar Médico" onclick="mAgregar()"><i class="fa fa-plus"></i></button></h2>
        <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
            <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
            <ul class="dropdown-menu" role="menu">
                <li><a href="#">Settings 1</a>
                </li>
                <li><a href="#">Settings 2</a>
                </li>
            </ul>
            </li>
            <li><a class="close-link"><i class="fa fa-close"></i></a>
            </li>
        </ul>
        <div class="clearfix"></div>
        </div>
        <div class="x_content" id="divMedico">
        <table id="datatable" class="table table-striped table-bordered">
            <thead>
            <tr>
                <th>CMP</th>
                <th>Nombres</th>
                <th>DNI</th>
                <th>Fec. Nacimiento</th>
                <th>Edad</th>
                <th>Especialidad</th>
                <th>Gestion</th>
            </tr>
            </thead>


            <tbody>
                @foreach ($medico as $m)
                <tr>
                    <td>{{ $m->numregistro }}</td>
                    <td>{{ $m->nombre }}{{ $m->apellido }}</td>
                    <td>{{ $m->dni }}</td>
                    <td>{{ $m->fecnacimiento }}</td>
                    <td>{{ $m->edad }}</td>
                    <td>{{ $m->especialidad }}</td>
                    <td><button type="button" class="btn btn-info btn-xs" data-toggle="tooltip" data-placement="top" title="Editar Médico"><i class="fa fa-pencil"></i></button><button type="button" class="btn btn-warning btn-xs" data-toggle="tooltip" data-placement="top" title="Dar de baja"><i class="fa fa-minus-circle"></i></button><button type="button" class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="top" title="Eliminar médico"><i class="fa fa-trash"></i></button></td>
                </tr>
                @endforeach
            </tbody>
        </table>
        </div>
    </div>
</div>

<!-- Modal Crear Paciente -->
<div id="crearMedico" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
            </button>
            <h4 class="modal-title" id="myModalLabel">Crear Nuevo Médico</h4>
        </div>
        <div class="modal-body row">
            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                <label class="control-label col-md-3 col-sm-3 col-xs-6">DNI</label>
                <input type="text" class="form-control has-feedback-left" id="dniNuevo" placeholder="Nuevo DNI">
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                <label class="control-label col-md-3 col-sm-3 col-xs-6">CMP</label>
                <input type="text" class="form-control has-feedback-left" id="cmpNuevo" placeholder="Nuevo CMP">
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                <label class="control-label col-md-3 col-sm-3 col-xs-6">Nombre</label>
                <input type="text" class="form-control has-feedback-left" id="nombreNuevo" placeholder="Ingrese Nombre">
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                <label class="control-label col-md-3 col-sm-3 col-xs-6">Apellido</label>
                <input type="text" class="form-control has-feedback-left" id="apellidoNuevo" placeholder="Ingrese Apellido">
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                <label class="control-label col-md-3 col-sm-3 col-xs-6">Direccion</label>
                <input type="text" class="form-control has-feedback-left" id="direccionNuevo" placeholder="Ingrese Dirección">
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                <label class="control-label col-md-3 col-sm-3 col-xs-6">Teléfono</label>
                <input type="text" class="form-control has-feedback-left" id="telefonoNuevo" placeholder="Ingresar Teléfono">
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                <label class="control-label col-md-6 col-sm-6 col-xs-6">Fecha de Nacimiento</label>
                <input type="date" class="form-control has-feedback-left" id="fecnacimientoNuevo">
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                <label class="control-label col-md-3 col-sm-3 col-xs-6">Edad</label>
                <input type="text" class="form-control has-feedback-left" id="edadNuevo" placeholder="Ingrese Edad">
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                <label class="control-label col-md-3 col-sm-3 col-xs-6">Género</label>
                <input type="text" class="form-control has-feedback-left" id="generoNuevo" placeholder="Ingresar Género">
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                <label class="control-label col-md-3 col-sm-3 col-xs-6">Correo</label>
                <input type="text" class="form-control has-feedback-left" id="correoNuevo" placeholder="Ingrese Correo">
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                <label class="control-label col-md-3 col-sm-3 col-xs-6">Especialidad</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    <select class="form-control" id="especialidadNuevo">
                      <option>Seleccione su especialidad...</option>
                      <option>Odontologia</option>
                    </select>
                  </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            <button type="button" class="btn btn-primary" onclick="nuevoMedico()">Guardar</button>
        </div>

        </div>
    </div>
</div>
<!-- Fin Modal Crear Paciente -->
@endsection
@section('script')
<script>

    function mAgregar() {
        $('#crearMedico').modal('show');
    }

    function nuevoMedico() {
        var dni = $('#dniNuevo').val();
        var cmp = $('#cmpNuevo').val();
        var nombre = $('#nombreNuevo').val();
        var apellido = $('#apellidoNuevo').val();
        var direccion = $('#direccionNuevo').val();
        var telefono = $('#telefonoNuevo').val();
        var fecnacimiento = $('#fecnacimientoNuevo').val();
        var edad = $('#edadNuevo').val();
        var genero = $('#generoNuevo').val();
        var correo = $('#correoNuevo').val();
        var espcialidad_id = $('#especialidadNuevo').val();

        $.post( "{{ Route('nuevoMedico') }}", {dni: dni, cmp: cmp, nombre: nombre, apellido: apellido, direccion: direccion, telefono: telefono, fecnacimiento: fecnacimiento, edad: edad, genero: genero, correo: correo, especialidad_id: especialidad_id, _token:'{{csrf_token()}}'}).done(function(data) {
                 $("#divMedico").empty();
                 $("#divMedico").html(data.view);        
                 $('#crearMedico').modal('hide');
            });
    }

</script>
    
@endsection