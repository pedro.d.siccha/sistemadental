@extends('layouts.app')
@section('nombrePagina')
 EMPRESA   
@endsection
@section('contenido')
<div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
        <div class="x_title">
        <h2>Empresa </h2>
        <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
            <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
            <ul class="dropdown-menu" role="menu">
                <li><a href="#">Settings 1</a>
                </li>
                <li><a href="#">Settings 2</a>
                </li>
            </ul>
            </li>
            <li><a class="close-link"><i class="fa fa-close"></i></a>
            </li>
        </ul>
        <div class="clearfix"></div>
        </div>
        <div class="x_content">
        <table id="datatable" class="table table-striped table-bordered">
            <thead>
            <tr>
                <th>Estado</th>
                <th>Nombre</th>
                <th>Fecha de Registro</th>
                <th>Gestion</th>
            </tr>
            </thead>


            <tbody>
                @foreach ($empresa as $e)
                <tr>
                    <td>{{ $e->estado }}</td>
                    <td>{{ $e->nombre }}</td>
                    <td>{{ $e->created_at }}</td>
                    <td><button type="button" class="btn btn-warning btn-xs" data-toggle="tooltip" data-placement="top" title="Dar de Baja"><i class="fa fa-minus-circle"></i></button><button type="button" class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="top" title="Eliminar Historia Clinica"><i class="fa fa-trash"></i></button></td>
                </tr>        
                @endforeach
            </tbody>
        </table>
        </div>
    </div>
</div>
@endsection