@extends('layouts.app')
@section('nombrePagina')
 Usuarios   
@endsection
@section('contenido')
<div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
        <div class="x_title">
        <h2>Lista de Usuarios <button type="button" class="btn btn-default btn-xs" data-toggle="tooltip" data-placement="top" title="Asignar Usuario"><i class="fa fa-plus"></i></button></h2>
        <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
            <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
            <ul class="dropdown-menu" role="menu">
                <li><a href="#">Settings 1</a>
                </li>
                <li><a href="#">Settings 2</a>
                </li>
            </ul>
            </li>
            <li><a class="close-link"><i class="fa fa-close"></i></a>
            </li>
        </ul>
        <div class="clearfix"></div>
        </div>
        <div class="x_content">
        <table id="datatable" class="table table-striped table-bordered">
            <thead>
            <tr>
                <th>Codigo</th>
                <th>Usuario</th>
                <th>Persona</th>
                <th>Correo</th>
                <th>Cargo</th>
                <th>Estado</th>
                <th>Gestion</th>
            </tr>
            </thead>


            <tbody>
                @foreach ($usuario as $u)
                <tr>
                    <td>{{ $u->users_id }}</td>
                    <td>{{ $u->name }}</td>
                    <td>{{ $u->nombre }} {{ $u->apellido }}</td>
                    <td>{{ $u->email }}</td>
                    <td>{{ $u->cargo }}</td>
                    <td>{{ $u->estado }}</td>
                    <td><button type="button" class="btn btn-primary btn-xs" data-toggle="tooltip" data-placement="top" title="Reinicar Contraseña"><i class="fa fa-refresh"></i></button><button type="button" class="btn btn-info btn-xs" data-toggle="tooltip" data-placement="top" title="Editar Usuario"><i class="fa fa-pencil"></i></button><button type="button" class="btn btn-warning btn-xs" data-toggle="tooltip" data-placement="top" title="Dar de baja"><i class="fa fa-minus-circle"></i></button><button type="button" class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="top" title="Eliminar usuario"><i class="fa fa-trash"></i></button></td>
                </tr>        
                @endforeach
            </tbody>
        </table>
        </div>
    </div>
</div>
@endsection